import React, { Component } from 'react';
import {
  Badge,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Dropdown
} from 'reactstrap';
import { connect } from "react-redux";
import { postLatestApi } from '../../actions/actions';
import { history } from '../../store';
import { setGlobalParam } from '../../reducers/reducers';
class HeaderDropdown extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  useLogout = (e) => {
    const { dispatch } = this.props;
    dispatch(postLatestApi({ api: 'login/logout', isAlert: false }));
    dispatch({ type: setGlobalParam , storage: 'is_authenticated', response: false });
    localStorage.removeItem('token');
    localStorage.removeItem('label');
    history.push('/login');
  }
  dropAccnt() {
    return (
      <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle nav>
          <i className="fa fa-user"></i>
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem><li onClick={this.useLogout} >Logout</li></DropdownItem>
        </DropdownMenu>
      </Dropdown>
    );
  }

  render() {
    const { ...attributes } = this.props;
    return (
      this.dropAccnt()
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

const mapStateToProps = state => {
  return {
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderDropdown);
