import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { getEveryApi } from '../../actions/actions';
import { setGlobalParam } from '../../reducers/reducers';
import treeChanges from 'tree-changes';
class Header extends PureComponent {
  render() {

    return (
      <header className="app-header navbar">
        {/* <NavbarToggler className="d-lg-none" onClick={this.mobileSidebarToggle}>
          <span className="navbar-toggler-icon"></span>
        </NavbarToggler>
        <NavbarBrand href="#"></NavbarBrand>
        <NavbarToggler className="d-md-down-none" onClick={this.sidebarToggle}>
          <span className="navbar-toggler-icon"></span>
        </NavbarToggler> */}
      </header>
    );
  }
}

export default connect(state => ({ ...state.reducer }), dispatch => ({ dispatch }))(Header);
