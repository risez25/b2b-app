import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Link } from "react-router-dom";
import classNames from 'classnames';
import nav from './_nav';

/*Home*/
import HomeIcon from "@material-ui/icons/Home";
import BoatIcon from "@material-ui/icons/DirectionsBoat";
import DashboardIcon from "@material-ui/icons/Dashboard";
import CalendarIcon from "@material-ui/icons/CalendarToday";
import AssignmentIcon from "@material-ui/icons/Assignment";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import BusinessIcon from "@material-ui/icons/Business";
import SupervisorAccountIcon from "@material-ui/icons/SupervisorAccount";
import InfoIcon from "@material-ui/icons/Info";
import RemoveFromQueueIcon from "@material-ui/icons/RemoveFromQueue";
import PlaylistAddIcon from "@material-ui/icons/PlaylistAdd";
import PlaylistAddCheckIcon from "@material-ui/icons/PlaylistAddCheck";
import PlaylistPlayIcon from "@material-ui/icons/PlaylistPlay";
import PermDataSettingIcon from "@material-ui/icons/PermDataSetting";
import PermContactCalendarIcon from "@material-ui/icons/PermContactCalendar";
import PermDeviceInformationIcon from "@material-ui/icons/PermDeviceInformation";
import LowPriorityIcon from "@material-ui/icons/LowPriority";
import DateRangeIcon from "@material-ui/icons/DateRange";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import Collapse from "@material-ui/core/Collapse";

function ListItemLink(props) {
  return <ListItem button component="li" {...props} />;
}


class Sidebar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      open1: false,
      open2: false
    };
  }

  handleClick = () => {
    this.setState(state => ({ open: !state.open }));
  };

  handleClick1 = () => {
    this.setState(state => ({ open1: !state.open1 }));
  };

  handleClick2 = () => {
    this.setState(state => ({ open2: !state.open2 }));
  };

  // handleClick(e) {
  //   e.preventDefault();
  //   e.target.parentElement.classList.toggle('open');
  // }

  // activeRoute(routeName, props) {
  //   // return this.props.location.pathname.indexOf(routeName) > -1 ? 'nav-item nav-dropdown open' : 'nav-item nav-dropdown';
  //   return props.location.pathname.indexOf(routeName) > -1 ? 'nav-item nav-dropdown open' : 'nav-item nav-dropdown';

  // }

  hideMobile() {
    if (document.body.classList.contains('sidebar-mobile-show')) {
      document.body.classList.toggle('sidebar-mobile-show')
    }
  }

  // todo Sidebar nav secondLevel
  // secondLevelActive(routeName) {
  //   return this.props.location.pathname.indexOf(routeName) > -1 ? "nav nav-second-level collapse in" : "nav nav-second-level collapse";
  // }


  render() {

    // sidebar-nav root
    return (
      <div className="side-menu">
        <List>
          <ListItemLink>
            <Link to="/">
              <ListItemIcon>
                <HomeIcon />
              </ListItemIcon>
              <ListItemText inset primary="Home" />
            </Link>
          </ListItemLink>
          <ListItemLink>
            <Link to="/myaccount">
              <ListItemIcon>
                <AccountCircleIcon />
              </ListItemIcon>
              <ListItemText inset primary="Account" />
            </Link>
          </ListItemLink>
        </List>
      </div>
    )
  }
}

export default Sidebar;
