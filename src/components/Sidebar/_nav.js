
export default {
    items: [
        {
            name: 'Dashboard',
            url: '/dashboard',
            icon: 'fa fa-dashboard',
            badge: {
                variant: 'info',
                text: 'NEW'
            }
        },
        {
            name: 'Sales',
            //url: '/OutbOrd/index',
            icon: 'fa fa-shopping-cart',
            children: [
                {
                    name: 'Debtors',
                    url: '/debtor/indexProcess/DEBTOR_LIST_01',
                    icon: 'fa fa-user-o',
                },
                {
                    name: 'Delivery Points',
                    url: '/deliveryPoint/indexProcess/DELIVERY_POINT_LIST_01',
                    icon: 'icon-location-pin',
                },
                {
                    name: 'Sales & Billing',
                    url: '/gdsDel/indexProcess/PICK_LIST_01',
                    icon: 'fa fa-arrow-right',
                },
                {
                    name: 'Return & Credit Note',
                    url: '/gdsDel/indexProcess/PICK_LIST_01',
                    icon: 'fa fa-arrow-left',
                },
                {
                    name: 'Documents',
                    icon: 'fa fa-folder-open-o',
                    children: [
                        {
                            name: 'Sales Order',
                            url: '/gdsDel/indexProcess/PICK_LIST_01',
                            icon: 'fa fa-folder-o',
                        },
                        {
                            name: 'Outbound Order',
                            url: '/outbOrd/index',
                            icon: 'fa fa-folder-o',
                        },
                        {
                            name: 'Delivery Order',
                            url: '/gdsDel/indexProcess/PICK_LIST_01',
                            icon: 'fa fa-folder-o',
                        },
                        {
                            name: 'Sales Invoice',
                            url: '/gdsDel/indexProcess/PICK_LIST_01',
                            icon: 'fa fa-folder-o',
                        }
                    ]
                },
            ]
        },
        {
            name: 'Purchase',
            //url: '/OutbOrd/index',
            icon: 'fa fa-shopping-basket',
            children: [
                {
                    name: 'Creditors',
                    url: '/creditor/indexProcess/CREDITOR_LIST_01',
                    icon: 'fa fa-user-o',
                },
                {
                    name: 'Purchasing',
                    url: '/gdsDel/indexProcess/PICK_LIST_01',
                    icon: 'fa fa-arrow-left',
                },
                {
                    name: 'Purchase Return',
                    url: '/gdsDel/indexProcess/PICK_LIST_01',
                    icon: 'fa fa-arrow-right',
                },
                {
                    name: 'Documents',
                    icon: 'fa fa-folder-open-o',
                    children: [
                        {
                            name: 'Sales Order',
                            url: '/gdsDel/indexProcess/PICK_LIST_01',
                            icon: 'fa fa-folder-o',
                        },
                        {
                            name: 'Outbound Order',
                            url: '/outbOrd/index',
                            icon: 'fa fa-folder-o',
                        },
                        {
                            name: 'Delivery Order',
                            url: '/gdsDel/indexProcess/PICK_LIST_01',
                            icon: 'fa fa-folder-o',
                        },
                        {
                            name: 'Sales Invoice',
                            url: '/gdsDel/indexProcess/PICK_LIST_01',
                            icon: 'fa fa-folder-o',
                        }
                    ]
                },
            ]
        },
        {
            name: 'Outbound',
            //url: '/OutbOrd/index',
            icon: 'fa fa-rocket',
            children: [
                {
                    name: 'Goods Delivery',
                    url: '/gdsDel/indexProcess/PICK_LIST_01',
                    icon: 'fa fa-arrow-right',
                },
                {
                    name: 'Return Receipt',
                    url: '/userList',
                    icon: 'fa fa-arrow-left',
                },
                {
                    name: 'Documents',
                    url: '/userList',
                    icon: 'fa fa-folder-open-o',
                },
            ]
        },
        {
            name: 'Inbound',
            //url: '/OutbOrd/index',
            icon: 'fa fa-truck',
            children: [
                {
                    name: 'Goods Receipt',
                    url: '/gdsRcpt/indexProcess/GDS_RCPT_01',
                    icon: 'fa fa-arrow-left',
                },
                {
                    name: 'Return Delivery',
                    url: '/userList',
                    icon: 'fa fa-arrow-right',
                },
                {
                    name: 'Documents',
                    url: '/userList',
                    icon: 'fa fa-folder-open-o',
                },
            ]
        },
        {
            name: 'Warehouse',
            //url: '/OutbOrd/index',
            icon: 'fa fa-industry',
            children: [
                {
                    name: 'Stock Items',
                    url: '/item/indexProcess/ITEM_LIST_01',
                    icon: 'fa fa-cubes',
                },
                {
                    name: 'Design & Layout',
                    url: '/storageBin/indexProcess/STORAGE_BIN_LIST_01',
                    icon: 'fa fa-building-o',
                },
                {
                    name: 'Documents',
                    url: '/userList',
                    icon: 'fa fa-folder-open-o',
                },
            ]
        },
        // {
        //     name: 'PickList',
        //     url: '/pickList/indexProcess/PICK_LIST_01',
        //     icon: 'icon-speedometer',
        // },
        {
            name: 'RBAC',
            url: '/RBAC/User',
            icon: 'fa fa-vcard-o',
        },
    ]
};
