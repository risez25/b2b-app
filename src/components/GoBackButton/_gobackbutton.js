import React, { Component, Suspense, lazy } from 'react';

//UI
import { Grid } from "@material-ui/core";
//libraray
import { Link } from "react-router-dom";
//component
import { Button } from '@material-ui/core';
class GoBackButton extends React.PureComponent {
    render() {
        const { to } = this.props;
        return (
            <React.Fragment>
                <Grid
                    style={{padding:10}}
                    container
                    spacing={16}
                    direction='row-reverse'
                >
                    <Button component={Link} to={to} className="go-back-button" variant='raised' >Go Back</Button>
                </Grid>
            </React.Fragment>
        )
    }
}
export default GoBackButton
