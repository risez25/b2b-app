import React, {Component} from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <span>&copy; Copyright 2019 <a href="http://luuwu.com">LuuWu.com</a>. All rights reserved.</span>
        <span className="ml-auto">Powered by <a href="http://luuwu.com">LuuWu Sdn. Bhd.</a></span>
      </footer>
    )
  }
}

export default Footer;


