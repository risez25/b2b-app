import React, { Component, Suspense, lazy } from 'react';
//UI
import {
    BottomNavigation,
    BottomNavigationAction,
} from "@material-ui/core";
import { Home, Receipt, ShoppingBasket, Description } from "@material-ui/icons";
//libraray
import { Link } from "react-router-dom";
//component
const component = 'TabBottomNavigation.js'
class TabBottomNavigation extends React.PureComponent {
    state = {
        value: ''
    }
    useOnChangeTab = (event, value) => {
        this.setState({ value: value });
    }
    render() {
        const { value } = this.state;
        return (
            <BottomNavigation className='layout-bottomnavigation bg-primary' value={value} onChange={this.useOnChangeTab} >
                <BottomNavigationAction className='layout-bottomnavigationaction' component={Link} to="/home" label="Home" value="/home" icon={<Home />} />
                <BottomNavigationAction className='layout-bottomnavigationaction' component={Link} to="/orderStatus" label="My Order" value="/orderStatus" icon={<ShoppingBasket />} />
                <BottomNavigationAction className='layout-bottomnavigationaction' component={Link} to="/invoiceHistory" label="Invoice History" value="/invoiceHistory" icon={<Receipt />} />
                <BottomNavigationAction className='layout-bottomnavigationaction' component={Link} to="/openBills" label="My Statement" value="openBills" icon={<Description />} />
            </BottomNavigation>
        )
    }
}
export default TabBottomNavigation;
