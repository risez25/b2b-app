/**
 * Wishlist NavLinks
 */
import React, { Fragment } from 'react';
import Button from '@material-ui/core/Button';
import Popover from '@material-ui/core/Popover';
import IconButton from "@material-ui/core/IconButton";
import Badge from "@material-ui/core/Badge";
import Favorite from "@material-ui/icons/Favorite";
//connect to store
import { connect } from "react-redux";

//action
// import { moveWishlistItemToCart, deleteItemFromWishlist } from '../../../actions/action'

// global components
import ConfirmationPopup from '../../global/confirmation-popup';
import CurrencyIcon from '../../global/currency/CurrencyIcon';
import { history } from '../../../store';
import Axios from 'axios';
import { endpoint } from '../../../api/api';

class Wishlist extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         anchorEl: null,
      };
      this.confirmationDialog = React.createRef();
   }


   /**
    * define function for open wishlist dropdown
    */
   handleClick = event => {
      this.setState({
         anchorEl: event.currentTarget,
      });
   };

   /**
    * define function for close wishlist dropdown
    */
   handleClose = () => {
      this.setState({
         anchorEl: null,
      });
   };

   useLogout = async (e) => {
      const { dispatch } = this.props;
      try {
         // http://localhost/scm/index.php?r=user/logout
         const response = await Axios({
            method: 'GET',
            url: `${endpoint}B2b/MobileLogout`,
            validateStatus: (status) => {
               console.log(status);
               return true;
            }
         });
         if (response.data.message == 'OK') {
            localStorage.removeItem('username');
            localStorage.removeItem('password');
            localStorage.removeItem('is_remember');
            history.push('./login')
         }
      } catch (error) {
         console.log(error);

      }
      
   }

   render() {

      const { anchorEl } = this.state;
      const open = Boolean(anchorEl);
      const { wishlist } = this.props;
      return (
         <div className="iron-whislist-wrap">
            <IconButton
               color="inherit"
               aria-label="whislist"
               aria-owns={open ? 'simple-popper' : null}
               aria-haspopup="true"
               variant="contained"
               onClick={this.handleClick}
               className="icon-btn mr-10"
            >
               <i className="material-icons">perm_identity</i>
            </IconButton>
            <Popover
               id="simple-popper"
               open={open}
               anchorEl={anchorEl}
               onClose={this.handleClose}
               anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'center',
               }}
               transformOrigin={{
                  vertical: 'top',
                  horizontal: 'center',
               }}
            >
               <div>
                  <Button onClick={this.useLogout}>
                     Logout
                     </Button>
               </div>
            </Popover>
         </div>
      );
   }
}

export default connect(
   state => ({
      user: state.reducer.ecommerce,
   }),
   dispatch => ({ dispatch })
)(Wishlist);
