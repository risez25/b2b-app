/**
 * cart list item
 */
/* eslint-disable */
import React, { Fragment } from 'react';
import Button from '@material-ui/core/Button';
import Popover from '@material-ui/core/Popover';
import IconButton from "@material-ui/core/IconButton";
import Badge from '@material-ui/core/Badge';
import { Link } from 'react-router-dom';

//connect to store
import { connect } from "react-redux";
import treeChanges from "tree-changes";

// global component
import { history } from '../../../store';
import { getEveryApi } from '../../../actions/actions';
const component = 'Cart'
class Cart extends React.Component {

   componentDidMount() {
      const { dispatch, division_data, division_value } = this.props;
      //user state
      const { customer_id, id, role, username } = this.props;
      let division_id = division_value == 0 ? division_data[0].id : division_value
      dispatch(getEveryApi({
         api: `B2b/GetCartItem&customer_id=${customer_id}&division_id=${division_id}`,
         storage: 'cart.cart_data',
         component: component
      }))
   }

   componentDidUpdate(nextProps) {
      const { dispatch } = this.props;
      const { changed } = treeChanges(this.props, nextProps);
      // if (changed('cart_data')) {
      //    dispatch(getEveryApi({
      //       api: `B2b/GetCartItem&customer_id=1&division_id=${nextProps.division_data[0].id}`,
      //       storage: 'cart.cart_data'
      //    }))
      // }
   }


   useGotoCart = () => {
      const { customer_id, id, role, username, division_value, division_data } = this.props;
      let division_id = division_value == 0 ? division_data[0].id : division_value
      history.push(`/cart/${customer_id}/${division_id}`);
   }

   render() {
      const { cart_data } = this.props;
      return (
         <div className="iron-cart-wrap">
            <IconButton
               aria-owns={open ? 'simple-popper' : null}
               aria-haspopup="true"
               variant="contained"
               onClick={this.useGotoCart}
               className="icon-btn mr-10"
               aria-label="Cart"
            >
               {cart_data && cart_data.length > 0 ?
                  (
                     <Badge
                        badgeContent={cart_data.length}
                        className="badge-active"
                     >
                        <i className="material-icons">shopping_cart</i>
                     </Badge>
                  )
                  :
                  (
                     <i className="material-icons">shopping_cart</i>
                  )
               }
            </IconButton>
         </div>
      );
   }
}

export default connect(
   state => ({
      ...state.reducer.cart,
      ...state.reducer.user,
      division_data: state.reducer.home.division_data,
      division_value: state.reducer.home.division_value
   }),
   dispatch => ({ dispatch })
)(Cart);