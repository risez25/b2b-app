import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import classNames from "classnames";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import Collapse from "@material-ui/core/Collapse";

/*Home*/
import HomeIcon from "@material-ui/icons/Home";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";

function ListItemLink(props) {
  return <ListItem button component="li" {...props} />;
}

class SideMenu extends React.Component {
  state = {
    open: false,
    open1: false,
    open2: false
  };

  handleClick = () => {
    this.setState(state => ({ open: !state.open }));
  };

  handleClick1 = () => {
    this.setState(state => ({ open1: !state.open1 }));
  };

  handleClick2 = () => {
    this.setState(state => ({ open2: !state.open2 }));
  };

  handleSecondMenu = () => {
    this.setState(state => ({ openSecond1: !state.openSecond1 }));
  };

  render() {
    const { openMenu } = this.props;

    return (
      <div className="side-menu">
        <List>
          <ListItemLink>
            <Link to="/">
              <ListItemIcon>
                <HomeIcon />
              </ListItemIcon>
              <ListItemText inset primary="Home" />
            </Link>
          </ListItemLink>
          <ListItemLink>
            <Link to="/myaccount">
              <ListItemIcon>
                <AccountCircleIcon />
              </ListItemIcon>
              <ListItemText inset primary="Account" />
            </Link>
          </ListItemLink>
        </List>
      </div>
    );
  }
}

export default SideMenu;
