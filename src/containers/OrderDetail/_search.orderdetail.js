import React, { Component, Suspense, lazy } from 'react';

//UI
import {
    Divider,
    Card,
    Grid,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    FilledInput,
    Paper,
    Typography,
    TextField,
    Button
} from "@material-ui/core";
import { DatePicker } from "material-ui-pickers";
//libraray
import { connect } from 'react-redux';
import { setGlobalParam } from '../../reducers/reducers';
import { getEveryApi, postEveryApi } from '../../actions/actions';
//component
const component = '_search.orderdetail.js'
class SearchOrderDetail extends React.PureComponent {
    useOnSelectDropdown = (e) => {
        const { dispatch, filter_id } = this.props;
        const { doc_code } = this.props.params;
        dispatch({
            type: setGlobalParam,
            storage: `orderdetail.filter_id`,
            response: e.target.value
        });

        dispatch(getEveryApi({
            api: `B2b/OrderDetails&doc_code=${doc_code}&filter_id=${e.target.value}`,
            storage: 'orderdetail.data',
            showLoadingStorage: 'orderdetail.is_table_loading'
        }))
    }

    render() {
        const { data, is_table_loading, filter_id } = this.props;
        return (
            <React.Fragment>
                <div className="search">
                    <form noValidate autoComplete="off">
                        <Grid container spacing={3} className="openbills-text" >
                            <Grid item xs>
                                <FormControl fullWidth>
                                    <InputLabel >Filter By</InputLabel>
                                    <Select
                                        value={filter_id}
                                        onChange={this.useOnSelectDropdown}
                                    >
                                        <MenuItem value={0}>Orders</MenuItem>
                                        <MenuItem value={1}>Sufficient</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </React.Fragment >
        )
    }
}
export default connect(
    state => ({
        ...state.reducer.orderdetail,
    }),
    dispatch => ({ dispatch })
)(SearchOrderDetail);
