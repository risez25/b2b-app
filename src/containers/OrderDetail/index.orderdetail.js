import React, { Component, Suspense, lazy } from 'react';

//UI
import {
    Grid,
    Typography,
    LinearProgress,
    Paper,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Divider
} from "@material-ui/core";
//libraray
import { connect } from 'react-redux';
import moment from "moment";
//component
import { getEveryApi } from '../../actions/actions';
import _tableOrderdetail from './_table.orderdetail';
import ContentLoader from '../../components/global/loaders/ContentLoader';
import _searchOrderdetail from './_search.orderdetail';
import _gobackbutton from "../../components/GoBackButton/_gobackbutton";
import { setGlobalParam } from '../../reducers/reducers';
const component = 'index.orderdetail.js'
class OrderDetail extends React.PureComponent {
    componentDidMount() {
        const { dispatch, division_value, division_data, id, filter_id } = this.props;
        const { doc_code } = this.props.match.params;
        dispatch(getEveryApi({
            api: `B2b/OrderDetails&doc_code=${doc_code}&filter_id=${filter_id}`,
            storage: 'orderdetail.data',
            showLoadingStorage: 'orderdetail.is_table_loading'
        }))
    }

    componentWillUnmount(){
        const {dispatch} =this.props;
        dispatch({
            type:setGlobalParam,
            storage:'orderdetail.data',
            response:null
        })
    }

    render() {
        const { data, is_table_loading } = this.props;
        return (
            <React.Fragment>
                {data !== null ?
                    <div className="orderdetail-page" >
                        <div className="container">
                            <_gobackbutton to="/orderStatus" />
                            <Paper className="page">

                                <_searchOrderdetail params={{ ...this.props.match.params }} />
                                {/* <_searchOrderStatus /> */}
                                <Divider />
                                <_tableOrderdetail />
                            </Paper>
                        </div>
                    </div>
                    :
                    <ContentLoader />
                }
            </React.Fragment>
        )
    }
}
export default connect(
    state => ({
        ...state.reducer.orderdetail,
        ...state.reducer.user,
        division_value: state.reducer.home.division_value,
        division_data: state.reducer.home.division_data
    }),
    dispatch => ({ dispatch })
)(OrderDetail);
