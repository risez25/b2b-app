import React, { Component, Suspense, lazy } from 'react';

//UI
import {
    Grid,
    Typography,
    LinearProgress,
    Paper,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Divider
} from "@material-ui/core";
//libraray
import { connect } from 'react-redux';
import moment from "moment";
//component
import { getEveryApi } from '../../actions/actions';
import ContentLoader from '../../components/global/loaders/ContentLoader';
import _gobackbutton from "../../components/GoBackButton/_gobackbutton";
import _tableInvoicedetail from './_table.invoicedetail';
const component = 'index.invoicedetail.js'
class InvoiceDetail extends React.PureComponent {
    componentDidMount() {

        const { dispatch } = this.props;
        const { inv_code } = this.props.match.params;
        dispatch(getEveryApi({
            api: `B2b/InvoiceDetails&inv_code=${inv_code}`,
            storage: 'invoicedetail.data',
            showLoadingStorage: 'invoicedetail.is_table_loading'
        }))
    }

    render() {
        const { data, is_table_loading } = this.props;
        return (
            <React.Fragment>
                {data !== null ?
                    <div className="invoicedetail-page" >
                        <div className="container">
                        <_gobackbutton to="/invoiceHistory"/>
                            <Paper className="page">
                                <_tableInvoicedetail />
                            </Paper>
                        </div>
                    </div>
                    :
                    <ContentLoader />
                }
            </React.Fragment>
        )
    }
}
export default connect(
    state => ({
        ...state.reducer.invoicedetail,
    }),
    dispatch => ({ dispatch })
)(InvoiceDetail);