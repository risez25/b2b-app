import React, { Component, Suspense, lazy } from 'react';

//UI
import {
    Grid,
    Typography,
    LinearProgress,
    Paper,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    TextField
} from "@material-ui/core";
//libraray
import { connect } from 'react-redux';
import moment from "moment";
//component
const component = '_table.invoicedetail.js'
class TableInvoiceDetail extends React.PureComponent {
    render() {
        const { data, is_table_loading } = this.props;
        const row_data = data;
        let total_net_amt = 0;
        let total_order_amt = 0;
        let total_tax_amt = 0;
        const renderTableData = row_data.map((value, index) => {
            const disc = value.disc_percent_01 > 0 ? parseFloat(value.disc_percent_01).toFixed(2) + '%' : '';
            total_net_amt += parseFloat(value.net_amt);
            total_order_amt += parseFloat(value.order_amt);
            total_tax_amt += parseFloat(value.tax_amt);
            return (
                <TableRow key={index}>
                    <TableCell>{value.item_barcode}</TableCell>
                    <TableCell>{value.item_long_description}</TableCell>
                    <TableCell>{parseFloat(value.qty).toFixed(0)}</TableCell>
                    <TableCell>{value.uom_code}</TableCell>
                    <TableCell>{parseFloat(value.price).toFixed(2)}</TableCell>
                    <TableCell>{disc}</TableCell>
                    <TableCell>{value.tax_code}</TableCell>
                    <TableCell>{parseFloat(value.order_amt).toFixed(2)}</TableCell>
                    <TableCell>{parseFloat(value.tax_amt).toFixed(2)}</TableCell>
                    <TableCell>{parseFloat(value.net_amt).toFixed(2)}</TableCell>
                </TableRow>
            )
        });

        return (
            <React.Fragment>
                <Paper className="table">
                    <Table>
                        <Typography>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Bar Code</TableCell>
                                    <TableCell>Item Description</TableCell>
                                    <TableCell>Qty</TableCell>
                                    <TableCell>Uom Code</TableCell>
                                    <TableCell>Price</TableCell>
                                    <TableCell>Disc Percent</TableCell>
                                    <TableCell>Tax Code</TableCell>
                                    <TableCell>Order Amt</TableCell>
                                    <TableCell>Tax Amt</TableCell>
                                    <TableCell>Net Amt</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>{renderTableData}</TableBody>
                        </Typography>
                    </Table>
                    <Grid container justify='flex-end' alignItems='center'>
                        <br />
                        <form noValidate autoComplete="off">
                            <Grid>
                                <TextField
                                    id="filled-name"
                                    label="Order Amt(RM)"
                                    value={parseFloat(total_order_amt).toFixed(2)}
                                    margin="normal"
                                    variant="filled"
                                />
                            </Grid>
                            <Grid>
                                <TextField
                                    id="filled-name"
                                    label="Tax Amt(RM)"
                                    value={parseFloat(total_tax_amt).toFixed(2)}
                                    margin="normal"
                                    variant="filled"
                                />
                            </Grid>
                            <Grid>
                                <TextField
                                    id="filled-name"
                                    label="Net Amt(RM)"
                                    value={parseFloat(total_net_amt).toFixed(2)}
                                    margin="normal"
                                    variant="filled"
                                />
                            </Grid> 
                        </form>
                    </Grid>
                </Paper>
            </React.Fragment>
        )
    }
}
export default connect(
    state => ({ ...state.reducer.invoicedetail }),
    dispatch => ({ dispatch })
)(TableInvoiceDetail);
