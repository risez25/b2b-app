import React, { Component, Suspense, lazy } from 'react';

//UI
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
//libraray
import { connect } from 'react-redux';
//component
import { setGlobalParam } from '../../reducers/reducers';
import { getEveryApi } from '../../actions/actions';
import ContentLoader from '../../components/global/loaders/ContentLoader';
import _detailCart from './_detail.cart';
import _branchCart from './_branch.cart';
import RctCard from '../../components/global/rct-card';
const component = 'index.cart.js'
class Cart extends Component {
    componentDidMount() {
        const { dispatch, division_value, division_data } = this.props;
        //user state
        const { customer_id, id, role, username } = this.props;
        let division_id = division_value == 0 ? division_data[0].id : division_value
        dispatch(getEveryApi({
            api: `B2b/GetCartItem&customer_id=${customer_id}&division_id=${division_id}`,
            storage: 'cart.cart_data',
            component: component
        }))
    }

    render() {
        const { cart_data } = this.props;
        return (
            <React.Fragment>
                {cart_data !== null ?
                    <RctCard className="cart-shop-list">
                        <div className="cart-page">
                            <_branchCart />
                            <_detailCart {...this.props.match} />
                        </div>
                    </RctCard>
                    :
                    <ContentLoader />
                }
            </React.Fragment>
        )
    }
}
export default connect(
    state => ({
        ...state.reducer.cart,
        ...state.reducer.user,
        division_value: state.reducer.home.division_value,
        division_data: state.reducer.home.division_data,
    }),
    dispatch => ({ dispatch })
)(Cart);
