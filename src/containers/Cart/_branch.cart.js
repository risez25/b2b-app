import React, { Component, Suspense, lazy } from 'react';

//UI
//libraray
import { connect } from 'react-redux';
//component
import { setGlobalParam } from '../../reducers/reducers';
import { getEveryApi } from '../../actions/actions';
import { Paper, FormControl, InputLabel, Select, FilledInput, MenuItem } from '@material-ui/core';
const component = '_branch.cart.js'
class Branch extends Component {
    componentDidMount() {
        const { dispatch, customer_id } = this.props;
        dispatch(getEveryApi({
            api: `B2b/getCustBranch&customer_id=${customer_id}`,
            storage: 'cart.branch_data',
            component: component
        }))
    }
    useSelectBranch = (e) => {
        const { dispatch } = this.props;
        dispatch({
            type: setGlobalParam,
            storage: 'cart.branch_id',
            response: e.target.value,
            component: component
        });
    }

    render() {
        const { branch_data, branch_id } = this.props;
        return (
            <Paper>
                {branch_data !== null ?
                    <FormControl variant="filled" fullWidth>
                        <InputLabel htmlFor="filled-age-simple">Branch</InputLabel>
                        <Select
                            value={branch_id}
                            onChange={this.useSelectBranch}
                            input={<FilledInput name="Branch" id="filled-Branch-simple" />}
                        >
                            {branch_data.map(option => (
                                <MenuItem key={option.id} value={option.id}>
                                    {option.code}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl> :
                    ''
                }
            </Paper>
        )
    }
}
export default connect(
    state => ({
        ...state.reducer.cart,
        ...state.reducer.user,
    }),
    dispatch => ({ dispatch })
)(Branch);
