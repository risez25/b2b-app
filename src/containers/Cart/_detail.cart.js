/**
 * Cart page
 */
/* eslint-disable */
import React, { Fragment } from 'react';

//Material ui
import { Button, Grid, Divider } from '@material-ui/core';
import { Link } from 'react-router-dom';

//connect to store
import { connect } from 'react-redux';
import treeChanges from "tree-changes";

//global component
import RctCard from '../../components/global/rct-card';
import ConfirmationPopup from '../../components/global/confirmation-popup';

// helpers
import { setGlobalParam } from '../../reducers/reducers';
import { delLatestApi, getEveryApi, postEveryApi } from '../../actions/actions';
const component = '_detail.cart.js'
class Cart extends React.Component {
    constructor(props) {
        super(props);
        this.confirmationDialog = React.createRef();
        this.state = {
            anchorEl: null,
        };
    }
    componentDidMount() {
        this.useCalculateTotalAmt();
    }

    componentWillReceiveProps(nextProps) {
        const { changedTo } = treeChanges(this.props, nextProps);
        const { dispatch, on_delete_item, division_value, division_data } = this.props
        //user state
        const { customer_id, id, role, username } = this.props;
        let division_id = division_value == 0 ? division_data[0].id : division_value
        if (changedTo('is_item_delete', false)) {
            dispatch(getEveryApi({
                api: `B2b/GetCartItem&customer_id=${customer_id}&division_id=${division_id}`,
                storage: 'cart.cart_data',
                showLoadingStorage: 'cart.is_cart_update',
                component: component
            }));
        }

        if (changedTo('is_item_update', false)) {
            dispatch(getEveryApi({
                api: `B2b/GetCartItem&customer_id=${customer_id}&division_id=${division_id}`,
                storage: 'cart.cart_data',
                showLoadingStorage: 'cart.is_cart_update',
                component: component
            }));
        }

        if (changedTo('is_cart_update', false)) {
            let totalPrice = 0;
            let totalPriceTax = 0;
            nextProps.cart_data.map((value, index) => {
                totalPrice += parseFloat(value.sum_amount);
                totalPriceTax += parseFloat(value.sum_amount);
            });

            dispatch({
                type: setGlobalParam,
                storage: 'cart.total_price',
                response: totalPrice,
                component: component
            });
            dispatch({
                type: setGlobalParam,
                storage: 'cart.total_price_tax',
                response: totalPriceTax,
                component: component
            });
            // this.useCalculateTotalAmt();
        }

        if (changedTo('is_place_order', false)) {
            dispatch({
                type: setGlobalParam,
                storage: 'cart.cart_data',
                response: [],
                component: component
            })
        }
    }

    useCalculateTotalAmt() {
        const { dispatch, cart_data } = this.props;
        let totalPrice = 0;
        let totalPriceTax = 0;
        cart_data.map((value, index) => {
            totalPrice += parseFloat(value.sum_amount);
            totalPriceTax += parseFloat(value.sum_amount);
        });

        dispatch({
            type: setGlobalParam,
            storage: 'cart.total_price',
            response: totalPrice,
            component: component
        });
        dispatch({
            type: setGlobalParam,
            storage: 'cart.total_price_tax',
            response: totalPriceTax,
            component: component
        });
    }
    useDecreaseQty = (qty, index) => (e) => {
        const { dispatch, cart_data } = this.props;
        let cartData = [...cart_data]
        if (qty != 1) {
            cartData[index].qty = parseInt(cartData[index].qty) - 1;
            dispatch(getEveryApi({
                api: `B2b/updateItemQtyInCart&id=${cartData[index].id}&qty=${cartData[index].qty}`,
                showLoadingStorage: 'cart.is_item_update',
                component: component
            }));
        }
    }

    useIncreaseQty = (qty, index) => (e) => {
        const { dispatch, cart_data } = this.props;
        let cartData = [...cart_data]
        cartData[index].qty = parseInt(cartData[index].qty) + 1;
        dispatch(getEveryApi({
            api: `B2b/updateItemQtyInCart&id=${cartData[index].id}&qty=${cartData[index].qty}`,
            showLoadingStorage: 'cart.is_item_update',
            component: component
        }));
    }
    useDeleteCartItem(popupResponse) {
        const { dispatch, on_delete_item, division_value, division_data } = this.props
        if (popupResponse) {
            dispatch(delLatestApi({
                api: `B2b/DeleteCartItem&item_id=${on_delete_item.item_id}&uom_id=${on_delete_item.uom_id}`,
                showLoadingStorage: 'cart.is_item_delete',
                isAlert: true,
                component: component
            }));

        }
        this.setState(
            {
                anchorEl: null,
            }
        )
    }
    useOnDeleteCartItem = (cartItem) => (e) => {
        const { dispatch } = this.props;
        dispatch({
            type: setGlobalParam,
            storage: 'cart.on_delete_item',
            response: cartItem,
            component: component
        })
        this.confirmationDialog.current.openDialog();
    }

    usePlaceOrder = (e) => {
        const { dispatch, division_value, division_data, cart_data, id, branch_id } = this.props;
        let user_id = id;
        let division_id = division_value == 0 ? division_data[0].id : division_value
        dispatch(getEveryApi({
            api: `B2b/PlaceOrder&division_id=${division_id}&user_id=${user_id}&cart_hdr_id=${cart_data[0].hdr_id}&branch_id=${branch_id}`,
            isAlert: true,
            showLoadingStorage: 'cart.is_place_order',
            component: component
        }))
    }
    render() {
        const { cart_data, total_price, total_price_tax } = this.props;
        return (
            <Fragment>
                <div className="iron-cart-wrapper bg-base">
                    <div className="inner-container section-pad">
                        <div className="container">
                            {(cart_data && cart_data.length > 0) ?
                                (
                                    <Fragment>
                                        <div>
                                            {cart_data && cart_data.map((cartItem, index) => {
                                                return (
                                                    <Fragment key={index}>
                                                        <Grid container spacing={24} className="my-0">
                                                            <Grid item xs={12} sm={6} md={3} lg={2} className="py-0 d-flex justify-content-center align-items-center mb-md-0 mb-20" >
                                                                <div className="text-center">
                                                                    <h5 className="mb-10">{cartItem.description} {cartItem.uom_code}</h5>
                                                                </div>
                                                            </Grid>
                                                            <Grid item xs={6} sm={6} md={2} lg={2} className="py-0 d-flex justify-content-center align-items-center mb-md-0 mb-20" >
                                                                RM {parseFloat(cartItem.price).toFixed(2)}
                                                            </Grid>
                                                            <Grid item xs={6} sm={4} md={2} lg={2} className="py-0 d-flex justify-content-center align-items-center" >
                                                                <div class="cart-info quantity">
                                                                    <div class="btn-increment-decrement"
                                                                        onClick={this.useDecreaseQty(cartItem.qty, index)}
                                                                    >-</div>
                                                                    <input class="input-quantity" id="input-quantity" value={parseInt(cartItem.qty).toFixed(0)} />
                                                                    <div class="btn-increment-decrement"
                                                                        onClick={this.useIncreaseQty(cartItem.qty, index)}
                                                                    >+</div>
                                                                </div>
                                                            </Grid>
                                                            <Grid item xs={6} sm={4} md={2} lg={2} className="py-0 d-flex justify-content-center align-items-center" >
                                                                RM {cartItem.sum_amount.toFixed(2)}
                                                            </Grid>
                                                            <Grid item xs={6} sm={4} md={1} lg={2} className="py-0 d-flex justify-content-center align-items-center" >
                                                                <Button
                                                                    className="cart-btn"
                                                                    onClick={this.useOnDeleteCartItem(cartItem)}
                                                                >
                                                                    <i className="zmdi zmdi-delete"></i>
                                                                </Button>
                                                            </Grid>
                                                        </Grid>
                                                        <Divider className="my-20" />

                                                    </Fragment>
                                                )
                                            })}
                                            <Grid container spacing={0} className="cart-total">
                                                <Grid item xs={12} sm={8} md={6} lg={5} className="ml-sm-auto">
                                                    <div className="d-flex justify-content-between align-items-center mb-15">
                                                        <span className="d-inline-block text-capitalize">subtotal</span>
                                                        <span>RM {parseFloat(total_price).toFixed(2)}</span>
                                                    </div>
                                                    <div className="d-flex justify-content-between align-items-center">
                                                        <span className="d-inline-block text-capitalize">Tax(GST)</span>
                                                        <span>RM 0</span>
                                                    </div>
                                                    <Divider className="my-20"></Divider>
                                                    <div className="d-flex justify-content-between align-items-center mb-20">
                                                        <h4>Total</h4>
                                                        <h4>RM {parseFloat(total_price_tax).toFixed(2)}</h4>
                                                    </div>
                                                    <div className="d-flex justify-content-end align-items-center">
                                                        <Button className="button btn-active btn-lg" onClick={this.usePlaceOrder}>Place Order</Button>
                                                    </div>
                                                </Grid>
                                            </Grid>
                                        </div>
                                    </Fragment>
                                )
                                :
                                (
                                    <div className="section-pad text-center">
                                        <div className="mb-30">
                                            <img src={require("../../assets/images/empty-cart.png")} alt="shop-cart" />
                                        </div>
                                        <h4>Your Shopping Bag is empty.</h4>
                                        <Link to='/home' className="text-capitalize">go for shopping</Link>
                                    </div>
                                )
                            }
                        </div>
                    </div>
                    <ConfirmationPopup
                        ref={this.confirmationDialog}
                        onConfirm={(res) => this.useDeleteCartItem(res)}
                    />
                </div>
            </Fragment>
        )
    }
}

export default connect(
    state => ({
        ...state.reducer.cart,
        ...state.reducer.user,
        division_value: state.reducer.home.division_value,
        division_data: state.reducer.home.division_data,
    }),
    dispatch => ({ dispatch })
)(Cart);