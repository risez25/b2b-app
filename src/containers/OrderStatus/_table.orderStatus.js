import React, { Component, Suspense, lazy } from 'react';
//UI
import {
    Grid,
    Typography,
    LinearProgress,
    Paper,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody
} from "@material-ui/core";
//libraray
import moment from "moment";
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
//component
import { dateFormat } from "../../constants/AppConfig";
const component = '_table.orderStatus.js'
class OrderStatus extends React.PureComponent {

    _renderStatus(status) {
        let value = ''
        switch (status) {
            case "0":
                value = 'Draft';
                break;
            case "3":
                value = 'WIP';
                break;
            case "4":
                value = 'Completed';
                break;
        }
        return (
            <div>
                {value}
            </div>
        )
    }
    render() {
        const { data, is_table_loading } = this.props;
        const renderTableData = data.map((value, index) => {
            return (
                <TableRow key={index}>
                    <TableCell><Link to={`/orderdetail/${value.details.doc_code}`}>{value.details.doc_code}</Link></TableCell>
                    <TableCell>{moment(value.doc_date).format(dateFormat)}</TableCell>
                    <TableCell>{this._renderStatus(value.details.status)}</TableCell>
                    <TableCell>{value.details.inv_no}</TableCell>
                    <TableCell>{parseFloat(value.details.order_amt).toFixed(2)}</TableCell>
                    <TableCell>{moment(value.details.shipment_delivered_date).format(dateFormat)}</TableCell>
                </TableRow>
            )
        });

        return (
            <React.Fragment>
                <Paper className="table">
                    <Typography>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Order No</TableCell>
                                    <TableCell>Order Date</TableCell>
                                    <TableCell>Status</TableCell>
                                    <TableCell>Inv No</TableCell>
                                    <TableCell>Net Amt</TableCell>
                                    <TableCell>Date Delivered</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>{renderTableData}</TableBody>
                        </Table>
                    </Typography>
                </Paper>
            </React.Fragment>
        )
    }
}
export default connect(
    state => ({ ...state.reducer.orderStatus }),
    dispatch => ({ dispatch })
)(OrderStatus);
