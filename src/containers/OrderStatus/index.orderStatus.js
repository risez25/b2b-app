import React, { Component, Suspense, lazy } from 'react';

//UI
import {
    Grid,
    Typography,
    LinearProgress,
    Paper,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Divider
} from "@material-ui/core";
//libraray
import { connect } from 'react-redux';
import moment from "moment";
import { Helmet  } from "react-helmet";
//component
import ContentLoader from '../../components/global/loaders/ContentLoader';
import { getEveryApi } from '../../actions/actions';
import { setGlobalParam } from '../../reducers/reducers';
import _tableOrderStatus from './_table.orderStatus';
import _searchOrderStatus from './_search.orderStatus';
const component = 'index.orderStatus.js'
class OrderStatus extends React.PureComponent {
    componentDidMount() {
        const { dispatch, division_value, division_data, id } = this.props;
        console.log(component);
        console.log(this.props);
        
        
        // const { division_id, group_type, group_id } = this.props.match.params;
        // http://localhost/scm/index.php?r=B2b/OrderStatusTracking&
        // user_id=5&
        // search_division_code=1,4&
        // date_from=2019-06-22&
        // date_to=2019-06-29&
        // search_user_code=&
        // doc_code=&
        // track_status=%27invoice%27&
        // search_customer_code=&
        // search_inv_no=

        let user_id = id;
        let start_of_week = moment().startOf('week');
        let end_of_week = moment().endOf('week');
        let search_division_code = division_value;
        if (division_value == 0) {
            search_division_code = division_data[0].division_id;
        }
        dispatch({
            type: setGlobalParam,
            storage: 'orderStatus.date_from',
            response: start_of_week
        })
        dispatch({
            type: setGlobalParam,
            storage: 'orderStatus.date_to',
            response: end_of_week
        })

        //get this week sales order
        dispatch(getEveryApi({
            api: `B2b/OrderStatusTracking&user_id=${user_id}&search_division_code=${search_division_code}&date_from=${start_of_week}&date_to=${end_of_week}&search_user_code=&doc_code=&track_status="invoice"&search_customer_code=&search_inv_no=`,
            storage: 'orderStatus.data',
            showLoadingStorage: 'orderStatus.is_table_loading',
            component: component
        }));

        // get sales order status dropdown
        dispatch(getEveryApi({
            api: `B2b/OrderStatusLookUp`,
            storage: 'orderStatus.order_status_dropdown',
            // showLoadingStorage: 'orderStatus.is_table_loading',
            component: component
        }));

    }
    componentWillUnmount() {
        // const { dispatch, default_state } = this.props;
        // let defaultState ={
        //     ...default_state, 
        //     default_state: default_state
        // }
        // dispatch({
        //     type: setGlobalParam,
        //     storage: 'product',
        //     response: defaultState,
        //     component:component
        // })
    }
    render() {
        const { data, is_table_loading } = this.props;
        return (
            <React.Fragment>
                <Helmet>
                    <title>My Order</title>
                </Helmet>
                {data !== null ?
                    <div className="orderStatus-page" >
                        <div className="container">
                            <Paper className="page">
                                <_searchOrderStatus />
                                <Divider />
                                <_tableOrderStatus />
                            </Paper>
                        </div>
                    </div>
                    :
                    <ContentLoader />
                }
            </React.Fragment>
        )
    }
}
export default connect(
    state => ({
        ...state.reducer.orderStatus,
        ...state.reducer.user,
        division_value: state.reducer.home.division_value,
        division_data: state.reducer.home.division_data
    }),
    dispatch => ({ dispatch })
)(OrderStatus);
