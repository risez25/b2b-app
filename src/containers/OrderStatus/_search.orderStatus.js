import React, { Component, Suspense, lazy } from 'react';

//UI
import {
    Divider,
    Card,
    Grid,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    FilledInput,
    Paper,
    Typography,
    TextField,
    Button
} from "@material-ui/core";
import { DatePicker } from "material-ui-pickers";
//libraray
import { connect } from 'react-redux';
import { setGlobalParam } from '../../reducers/reducers';
import { getEveryApi } from '../../actions/actions';
//component
const component = '_search.orderStatus.js'

class OrderStatus extends React.PureComponent {
    useOnDateChange = (field) => (date_moment) => {
        const { dispatch } = this.props;
        dispatch({
            type: setGlobalParam,
            storage: `orderStatus.${field}`,
            response: date_moment
        })
    }

    useOnChangeValue = (field) => (e) => {
        const { dispatch } = this.props;
        dispatch({
            type: setGlobalParam,
            storage: `orderStatus.${field}`,
            response: e.target.value
        })
    }
    useOnSelectDropdown = (field) => (e) => {
        const { dispatch } = this.props;
        dispatch({
            type: setGlobalParam,
            storage: `orderStatus.${field}`,
            response: e.target.value
        })
    }
    useOnSearch = (e) => {
        const { dispatch, id } = this.props;
        // state for search
        const { date_from, date_to, search_division_code, doc_code, track_status, search_inv_no } = this.props;
        let user_id = id;
        dispatch(getEveryApi({
            api: `B2b/OrderStatusTracking&user_id=${user_id}&search_division_code=${search_division_code}&date_from=${date_from.format('YYYY-MM-DD')}&date_to=${date_to.format('YYYY-MM-DD')}&search_user_code=&doc_code=${doc_code}&track_status=${track_status}&search_customer_code=&search_inv_no=${search_inv_no}`,
            storage: 'orderStatus.data',
            showLoadingStorage: 'orderStatus.is_table_loading',
            component: component
        }));
    }
    render() {
        const { data, is_table_loading, date_from, order_status_dropdown, date_to, search_division_code, division_data, doc_code, track_status, search_inv_no } = this.props;
        return (
            <React.Fragment>
                <div className="search">
                    <form noValidate autoComplete="off">
                        <Grid container spacing={3}>
                            <FormControl variant="filled" fullWidth>
                                <InputLabel htmlFor="filled-age-simple">Division</InputLabel>
                                <Select
                                    value={search_division_code}
                                    onChange={this.useOnSelectDropdown('search_division_code')}
                                    input={<FilledInput name="Division" id="filled-division-simple" />}
                                >
                                    {division_data.map(option => (
                                        <MenuItem key={option.id} value={option.id}>
                                            {option.code}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid container spacing={3}>
                            <Grid item xs>
                                <FormControl variant="filled" fullWidth>
                                    <DatePicker
                                        label="Date From"
                                        format="DD-MM-YYYY"
                                        value={date_from}
                                        onChange={this.useOnDateChange('date_from')}
                                        animateYearScrolling
                                    />
                                </FormControl>
                            </Grid>
                            <Grid item xs={1}><Typography align="center" >-</Typography> </Grid>
                            <Grid item xs>
                                <FormControl variant="filled" fullWidth>
                                    <DatePicker
                                        label="Date To"
                                        format="DD-MM-YYYY"
                                        value={date_to}
                                        onChange={this.useOnDateChange('date_to')}
                                        animateYearScrolling
                                    />
                                </FormControl>
                            </Grid>
                        </Grid>
                        <Grid container spacing={3}>
                            <Grid item xs>
                                <FormControl variant="filled" fullWidth>
                                    <InputLabel htmlFor="dropdown-track-status">S/O Status</InputLabel>
                                    <Select
                                        value={track_status}
                                        onChange={this.useOnSelectDropdown('track_status')}
                                        input={<FilledInput name="S/O Status" id="dropdown-track-status" />}
                                    >
                                        {
                                            order_status_dropdown.map((value, index) => {
                                                if (value.key == 0 || value.key == 3 || value.key == 4) {
                                                    return (
                                                        <MenuItem value={`${value.key}`}>{value.label}</MenuItem>
                                                    )
                                                }
                                            })
                                        }
                                    </Select>
                                </FormControl>
                            </Grid>
                        </Grid>
                        <Grid container spacing={3}>
                            <Grid item xs>
                                <FormControl variant="filled" fullWidth>
                                    <TextField
                                        label="S/O No"
                                        value={doc_code}
                                        onChange={this.useOnChangeValue('doc_code')}
                                        margin="normal"
                                    />
                                </FormControl>
                            </Grid>
                            <Grid xs={1} />
                            <Grid item xs>
                                <FormControl variant="filled" fullWidth>
                                    <TextField
                                        label="Invoice No"
                                        value={search_inv_no}
                                        onChange={this.useOnChangeValue('search_inv_no')}
                                        margin="normal"
                                    />
                                </FormControl>
                            </Grid>
                        </Grid>
                        <Grid
                            container
                            direction="row"
                            justify="flex-end"
                            alignItems="center"
                        >
                            <Button color="secondary" onClick={this.useOnSearch}>
                                Search
                            </Button>
                        </Grid>
                    </form>
                </div>
            </React.Fragment >
        )
    }
}
export default connect(
    state => ({
        ...state.reducer.orderStatus,
        ...state.reducer.user,
        division_data: state.reducer.home.division_data
    }),
    dispatch => ({ dispatch })
)(OrderStatus);
