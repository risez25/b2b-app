import React, { Component, Suspense, lazy } from 'react';

//UI
import {
    Grid,
    Typography,
    LinearProgress,
    Paper,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Divider
} from "@material-ui/core";
//libraray
import { connect } from 'react-redux';
import { Helmet } from "react-helmet";
//component
import ContentLoader from '../../components/global/loaders/ContentLoader';
import { getEveryApi, postEveryApi } from '../../actions/actions';
import { setGlobalParam } from '../../reducers/reducers';
import _tableOpenbills from './_table.openbills';
import _searchOpenbills from './_search.openbills';
const component = 'index.openbills.js'
class OpenBills extends React.PureComponent {
    componentDidMount() {
        const { dispatch, id } = this.props;
        let user_id = id;
        let bodyFormData = new FormData();
        bodyFormData.set('openBills', 'openBills');
        dispatch(postEveryApi({
            api: `B2b/QueryAging&user_id=${user_id}`,
            data: bodyFormData,
            storage: 'openbills.data',
            component: component
        }))
    }
    componentWillUnmount() {
        // const { dispatch, default_state } = this.props;
        // let defaultState ={
        //     ...default_state, 
        //     default_state: default_state
        // }
        // dispatch({
        //     type: setGlobalParam,
        //     storage: 'product',
        //     response: defaultState,
        //     component:component
        // })
    }
    render() {
        const { data } = this.props;
        return (
            <React.Fragment>
                <Helmet>
                    <title>My Statement</title>
                    <link rel="canonical" href="http://mysite.com/example" />
                </Helmet>
                {data !== null ?
                    <div className="openbills-page" >
                        <div className="container">
                            <Paper>
                                <Typography>
                                    <_searchOpenbills />
                                    <Divider />
                                    <_tableOpenbills />
                                </Typography>
                            </Paper>
                        </div>
                    </div>
                    :
                    <ContentLoader />
                }
            </React.Fragment>
        )
    }
}
export default connect(
    state => ({
        ...state.reducer.openbills,
        ...state.reducer.user
    }),
    dispatch => ({ dispatch })
)(OpenBills);
