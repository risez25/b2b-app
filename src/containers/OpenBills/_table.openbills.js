import React, { Component, Suspense, lazy } from 'react';

//UI
import {
    Grid,
    Typography,
    LinearProgress,
    Paper,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody
} from "@material-ui/core";
//libraray
import { connect } from 'react-redux';
import moment from "moment";
//component
import ContentLoader from '../../components/global/loaders/ContentLoader';
import { getEveryApi } from '../../actions/actions';
import { setGlobalParam } from '../../reducers/reducers';
import { dateFormat } from '../../constants/AppConfig';
const component = '_table.openbills.js'
class OrderStatus extends React.PureComponent {
    render() {
        const { data, is_table_loading } = this.props;

        const row_data = data.tabs.data.dataProvider.rawData;
        
        const renderTableData = row_data.map((value, index) => {
            let bill_amt = (-1* value.sign * (value.local_amt))
            let balance = (-1* value.sign * (value.local_amt - value.paid_local_amt))
            // (-1* $data->sign * ($data->local_amt)) bill amt
            return (
                <TableRow key={index}>
                    <TableCell>{moment(value.doc_date).format(dateFormat)}</TableCell>
                    <TableCell>{value.due_date}</TableCell>
                    <TableCell>{value.doc_type}</TableCell>
                    <TableCell>{value.doc_no}</TableCell>
                    <TableCell>{value.ref_no_01}</TableCell>
                    <TableCell>{parseFloat(bill_amt).toFixed(2)}</TableCell>
                    <TableCell>{parseFloat(balance).toFixed(2)}</TableCell>
                    <TableCell>{parseFloat(data.amt_data[index].cum_balance_all).toFixed(2)}</TableCell>
                </TableRow>
            )
        });

        return (
            <React.Fragment>
                <Paper className="table">
                    <Table>
                        <Typography>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Date</TableCell>
                                    <TableCell>Due Date</TableCell>
                                    <TableCell>Type</TableCell>
                                    <TableCell>Doc No.</TableCell>
                                    <TableCell>Ref No.</TableCell>
                                    <TableCell>Bill Amt(RM)</TableCell>
                                    <TableCell>Balance(RM)</TableCell>
                                    <TableCell>Cum Balance(RM)</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>{renderTableData}</TableBody>
                        </Typography>
                    </Table>
                </Paper>
            </React.Fragment>
        )
    }
}
export default connect(
    state => ({ ...state.reducer.openbills }),
    dispatch => ({ dispatch })
)(OrderStatus);
