import React, { Component, Suspense, lazy } from 'react';

//UI
import {
    Divider,
    Card,
    Grid,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    FilledInput,
    Paper,
    Typography,
    TextField,
    Button
} from "@material-ui/core";
import { DatePicker } from "material-ui-pickers";
//libraray
import { connect } from 'react-redux';
import { setGlobalParam } from '../../reducers/reducers';
import { getEveryApi, postEveryApi } from '../../actions/actions';
//component
const component = '_search.openbills.js'
class SearchOpenBills extends React.PureComponent {
    useOnSelectDropdown = (field) => (e) => {
        const { dispatch } = this.props;
        dispatch({
            type: setGlobalParam,
            storage: `invoiceHistory.${field}`,
            response: e.target.value
        })
    }
    useOnSearch = (e) => {
        const { dispatch, division_id, id } = this.props;
        let user_id = id;
        dispatch(getEveryApi({
            api: `B2b/InvoiceHistory&user_id=${user_id}&division_id=${division_id}`,
            storage: 'invoiceHistory.data',
            showLoadingStorage: 'invoiceHistory.is_table_loading',
            component: component
        }));
    }

    useOnPickMonth(month) {
        const { dispatch, id } = this.props;
        let user_id = id;
        let bodyFormData = new FormData();
        bodyFormData.set(month, month);
        dispatch(postEveryApi({
            api: `B2b/QueryAging&user_id=${user_id}`,
            data: bodyFormData,
            storage: 'openbills.data',
            component: component
        }))
    }
    render() {
        const { data, is_table_loading, division_id, division_data } = this.props;
        return (
            <React.Fragment>
                <div className="search">
                    <form noValidate autoComplete="off">
                        <Grid container spacing={3}>
                            <Button onClick={() => this.useOnPickMonth('twelveMonth')}>12 Months</Button>
                            <Button onClick={() => this.useOnPickMonth('sixMonth')}>6 Months</Button>
                            <Button onClick={() => this.useOnPickMonth('threeMonth')}>3 Months</Button>
                            <Button onClick={() => this.useOnPickMonth('oneMonth')}>1 Month</Button>
                            <Button onClick={() => this.useOnPickMonth('openBills')}>Open bills</Button>
                        </Grid>
                        <Divider />
                        <Grid container spacing={3} className="openbills-text" >
                            <Grid item xs>
                                <FormControl variant="filled">
                                    <TextField
                                        label="Term"
                                        value={data.termsDetail.code}
                                        margin="normal"
                                        disabled
                                    />
                                </FormControl>
                            </Grid>
                            <Grid item xs>
                                <FormControl variant="filled">
                                    <TextField
                                        label="Credit Limit"
                                        value={parseFloat(data.customerModel.credit_limit).toFixed(2)}
                                        margin="normal"
                                        disabled
                                    />
                                </FormControl>
                            </Grid>
                        </Grid>
                        <Grid container spacing={3} className="openbills-text" >
                            <Grid item xs>
                                <FormControl variant="filled">
                                    <TextField
                                        label="Balance"
                                        value={data.customerDetail.overdueBalance}
                                        margin="normal"
                                        disabled
                                    />
                                </FormControl>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </React.Fragment >
        )
    }
}
export default connect(
    state => ({
        ...state.reducer.openbills,
        ...state.reducer.user,
        division_data: state.reducer.home.division_data
    }),
    dispatch => ({ dispatch })
)(SearchOpenBills);
