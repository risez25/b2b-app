/**
 * post detail component
*/
/* eslint-disable */
import React from 'react';

//UI
import { Grid, Button } from '@material-ui/core';

//library
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import treeChanges from "tree-changes";

//components
import { setGlobalParam } from '../../reducers/reducers';
import { postEveryApi, getEveryApi } from '../../actions/actions';
import Swal from 'sweetalert2';
import NoImage from "../../assets/images/NoImage.jpg";
const component = '_detail.productDetail'
class Detail extends React.PureComponent {
    componentDidMount() {
        const { dispatch, product_detail_data } = this.props;
        let product_unit = [];
        product_detail_data.item_details.map((value, index) => {
            product_unit.push({ ...value, qty: 0 });
        })
        dispatch({
            type: setGlobalParam,
            storage: 'productDetail.product_unit',
            response: product_unit,
            component: component
        })
    }

    componentWillReceiveProps(nextProps) {
        const { changedTo } = treeChanges(this.props, nextProps);
        const { dispatch, division_data, division_value } = this.props;
        //user state
        const { customer_id, id, role, username } = this.props;
        if (changedTo('is_add_to_cart', false)) {
            let division_id = division_value == 0 ? division_data[0].id : division_value
            dispatch(getEveryApi({
                api: `B2b/GetCartItem&customer_id=${customer_id}&division_id=${division_id}`,
                storage: 'cart.cart_data',
                component: component
            }))
        }
    }

    useDecreaseQty = (qty, index) => (e) => {
        const { dispatch, product_unit } = this.props;
        let productUnit = [...product_unit]
        if (qty != 0) {
            productUnit[index].qty -= 1;
            dispatch({
                type: setGlobalParam,
                storage: 'productDetail.product_unit',
                response: productUnit,
                component: component
            })
        }
    }

    useIncreaseQty = (qty, index) => (e) => {
        const { dispatch, product_unit } = this.props;
        let productUnit = [...product_unit]
        productUnit[index].qty += 1;
        dispatch({
            type: setGlobalParam,
            storage: 'productDetail.product_unit',
            response: productUnit,
            component: component
        })
    }

    useAddToCart = (e) => {
        const { dispatch, product_unit, division_value } = this.props;
        const { item_id, group_type, group_id } = this.props.params;
        //user state
        const { customer_id, id, role, username } = this.props;
        let productUnit = [];
        product_unit.map((value, index) => {
            if (value.qty != 0) {
                productUnit.push(value);
            }
        })
        let bodyFormData = new FormData();
        let division_id = division_value == 0 ? division_data[0].id : division_value
        if (productUnit.length > 0) {
            bodyFormData.set('CartForm[customer_id]', customer_id);
            bodyFormData.set('CartForm[division_id]', division_id);
            bodyFormData.set('CartForm[productUnit]', JSON.stringify(productUnit));
            dispatch(postEveryApi({
                api: 'B2b/PostCart',
                data: bodyFormData,
                showLoadingStorage: 'productDetail.is_add_to_cart',
                isAlert: true,
                component: component
            }));
        } else {
            Swal.fire(
                '',
                'you need to pick at least one time',
                'error'
            )
        }


    }
    render() {
        const { product_detail_data, product_unit } = this.props;
        const { item_id, division_id, group_type, group_id } = this.props.params;
        return (
            <div>
                <Grid container spacing={32} className="my-0">
                    <Grid item xs={12} sm={12} md={6} lg={6} className="py-0 mb-md-0 mb-sm-30">
                        <Grid container spacing={24} className="iron-product-gallery">
                            <Grid item xs={9} sm={10} md={10} lg={10}>
                                <div className="preview-full-image">
                                    <div className="iron-shadow product-gallery-item ">
                                        <div>
                                            <a href="javascript:void(0)">
                                                <img
                                                    // src={require(`../../assets/images/accessroies/a-2-b.jpg`)}
                                                    src={ product_detail_data.picture !=''? `data:image/png;base64,${product_detail_data.picture}` : require('../../assets/images/NoImage.jpg')}
                                                />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} lg={6} className="py-0">
                        <div className="detail-content">
                            <Link to={`/product/${division_id}/${group_type}/${group_id}`} className="text-14 d-inline-block mb-10">
                                <Button>
                                    Back to shop
                                </Button>
                            </Link>
                            <h3>{product_detail_data.code}</h3>
                            {
                                product_unit.map((value, index) => {
                                    return (
                                        <Grid container spacing={3} direction="row">
                                            <Grid item xs={4}>
                                                <div>{value.userfield_01}</div>
                                            </Grid>

                                            <Grid item xs={4}>
                                                <div class="cart-info quantity">
                                                    <div class="btn-increment-decrement" onClick={this.useDecreaseQty(value.qty, index)}>-</div>
                                                    <input class="input-quantity" id="input-quantity" value={value.qty} />
                                                    <div class="btn-increment-decrement" onClick={this.useIncreaseQty(value.qty, index)}>+</div>
                                                </div>
                                            </Grid>

                                            <Grid item xs={4}>
                                                <h4 className="active-color">RM {parseFloat(value.usernumber_01).toFixed(2)}</h4>
                                            </Grid>
                                        </Grid>
                                    )
                                })
                            }
                            <div className="short-desc">
                                <p>{product_detail_data.description}</p>
                            </div>
                            <div className="mb-sm-50 mb-20 detail-btns">
                                <Button
                                    className="button btn-active btn-lg mr-15 mb-20 mb-sm-0"
                                    onClick={this.useAddToCart}
                                >
                                    add to cart
                                        </Button>
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </div >
        )
    }
}
export default connect(
    state => ({
        ...state.reducer.productDetail,
        ...state.reducer.user,
        division_value: state.reducer.home.division_value,
        division_data: state.reducer.home.division_data
    }),
    dispatch => ({ dispatch })
)(Detail);