import React, { Component, Suspense, lazy } from 'react';

//UI
import Grid from "@material-ui/core/Grid";
//libraray
import { connect } from 'react-redux';
//component
import ContentLoader from '../../components/global/loaders/ContentLoader';
import { getEveryApi } from '../../actions/actions';
import _detailProductDetail from './_detail.productDetail';
import { setGlobalParam } from '../../reducers/reducers';
const component = 'index.productDetail'
class ProductDetail extends React.PureComponent {
    componentDidMount() {
        const { dispatch } = this.props;
        const { item_id, division_id, group_type, group_id } = this.props.match.params;
        //user state
        const { customer_id, id, role, username } = this.props;
        dispatch(getEveryApi({
            api: `B2b/GetPoductdetail&item_id=${item_id}&division_id=${division_id}&customer_id=${customer_id}`,
            storage: 'productDetail.product_detail_data',
            component:component
        }))
    }
    componentWillUnmount() {
        const { dispatch, default_state } = this.props;
        let defaultState ={
            ...default_state, 
            default_state: default_state
        }
        dispatch({
            type: setGlobalParam,
            storage: 'productDetail',
            response: defaultState,
            component:component
        })
    }
    render() {
        const { product_detail_data } = this.props;
        return (
            <React.Fragment>
                {product_detail_data !== null ?
                    <div className="product-detail-page" >
                        <div className="inner-container">
                            <div className="bg-base section-pad">
                                <div className="container">
                                    <Grid container spacing={0}>
                                        <Grid item lg={12} className="mx-auto">
                                            <_detailProductDetail {...this.props.match}/>
                                            {/* {currentDataItem !== null &&
                                                <PostDetail
                                                    data={currentDataItem}
                                                />
                                            } */}
                                        </Grid>
                                    </Grid>
                                </div>
                            </div>
                        </div>
                    </div>
                    :
                    <ContentLoader />
                }
            </React.Fragment>
        )
    }
}
export default connect(
    state => ({ 
        ...state.reducer.productDetail,
        ...state.reducer.user
    }),
    dispatch => ({ dispatch })
)(ProductDetail);