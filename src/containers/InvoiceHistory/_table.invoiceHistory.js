import React, { Component, Suspense, lazy } from 'react';

//UI
import {
    Grid,
    Typography,
    LinearProgress,
    Paper,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody
} from "@material-ui/core";
//libraray
import { connect } from 'react-redux';
import moment from "moment";
import { dateFormat } from '../../constants/AppConfig';
import { Link } from "react-router-dom";
//component

const component = '_table.invoiceHistory.js'
class TableInvoice extends React.PureComponent {
    render() {
        const { data, is_table_loading } = this.props;
        const renderTableData = data.map((value, index) => {
            return (
                <TableRow key={index}>
                    <TableCell><Link to={`/invoicedetail/${value.inv_code}`}>{value.inv_code}</Link></TableCell>
                    <TableCell>{moment(value.doc_date).format(dateFormat)}</TableCell>
                    <TableCell>{parseFloat(value.net_amt).toFixed(2)}</TableCell>
                </TableRow>
            )
        });

        return (
            <React.Fragment>
                <Paper className="table">
                    <Typography>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Doc Code</TableCell>
                                    <TableCell>Doc Date</TableCell>
                                    <TableCell>Net Amount</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>{renderTableData}</TableBody>
                        </Table>
                    </Typography>
                </Paper>
            </React.Fragment>
        )
    }
}
export default connect(
    state => ({ ...state.reducer.invoiceHistory }),
    dispatch => ({ dispatch })
)(TableInvoice);
