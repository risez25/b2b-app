import React, { Component, Suspense, lazy } from 'react';

//UI
import {
    Divider,
    Card,
    Grid,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    FilledInput,
    Paper,
    Typography,
    TextField,
    Button
} from "@material-ui/core";
import { DatePicker } from "material-ui-pickers";
//libraray
import { connect } from 'react-redux';
import { setGlobalParam } from '../../reducers/reducers';
import { getEveryApi } from '../../actions/actions';
//component
const component = '_search.invoiceHistory.js'

class SearchInvoice extends React.PureComponent {

    useOnSelectDropdown = (field) => (e) => {
        const { dispatch } = this.props;
        dispatch({
            type: setGlobalParam,
            storage: `invoiceHistory.${field}`,
            response: e.target.value
        })
    }
    useOnSearch = (e) => {
        const { dispatch, division_id } = this.props;
        dispatch(getEveryApi({
            api: `B2b/InvoiceHistory&user_id=5&division_id=${division_id}`,
            storage: 'invoiceHistory.data',
            showLoadingStorage: 'invoiceHistory.is_table_loading',
            component: component
        }));
    }
    render() {
        const { data, is_table_loading, division_id, division_data} = this.props;
        return (
            <React.Fragment>
                <div className="search">
                    <form noValidate autoComplete="off">
                        <Grid container spacing={3}>
                            <FormControl variant="filled" fullWidth>
                                <InputLabel htmlFor="filled-age-simple">Division</InputLabel>
                                <Select
                                    value={division_id}
                                    onChange={this.useOnSelectDropdown('division_id')}
                                    input={<FilledInput name="Division" id="filled-division-simple" />}
                                >
                                    {division_data.map(option => (
                                        <MenuItem key={option.id} value={option.id}>
                                            {option.code}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid
                            container
                            direction="row"
                            justify="flex-end"
                            alignItems="center"
                        >
                            <Button color="secondary" onClick={this.useOnSearch}>
                                Search
                            </Button>
                        </Grid>
                    </form>
                </div>
            </React.Fragment >
        )
    }
}
export default connect(
    state => ({
        ...state.reducer.invoiceHistory,
        division_data: state.reducer.home.division_data
    }),
    dispatch => ({ dispatch })
)(SearchInvoice);
