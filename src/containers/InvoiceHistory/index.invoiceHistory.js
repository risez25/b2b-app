import React, { Component, Suspense, lazy } from 'react';

//UI
import { Paper, Typography, Grid, Divider } from '@material-ui/core';
//libraray
import { connect } from 'react-redux';
import { Helmet } from "react-helmet";
//component
import ContentLoader from '../../components/global/loaders/ContentLoader';
import { getEveryApi } from '../../actions/actions';
import { setGlobalParam } from '../../reducers/reducers';

import _tableInvoiceHistory from './_table.invoiceHistory';
import _searchInvoiceHistory from './_search.invoiceHistory';
const component = 'index.invoiceHistory';
class Invoice extends React.PureComponent {
    componentDidMount() {
        const { dispatch, id, division_value, division_data } = this.props;
        let user_id = id;
        let division_id = division_value == 0 ? division_data[0].id : division_value
        dispatch(getEveryApi({
            api: `B2b/InvoiceHistory&user_id=${user_id}&division_id=${division_id}`,
            storage: 'invoiceHistory.data',
            component: component
        }))
    }
    componentWillUnmount() {
        const { dispatch, default_state } = this.props;
        let defaultState = {
            ...default_state,
            default_state: default_state
        }
        // dispatch({
        //     type: setGlobalParam,
        //     storage: 'product',
        //     response: defaultState,
        //     component:component
        // })
    }
    render() {
        const { data } = this.props;
        return (
            <React.Fragment>
                <Helmet>
                    <title>Invoice History</title>
                </Helmet>
                {data !== null ?
                    <div className="invoice-page" >
                        <div className="container">
                            <Paper>
                                <Typography>
                                    <_searchInvoiceHistory />
                                    <Divider />
                                    <_tableInvoiceHistory />
                                </Typography>
                            </Paper>

                        </div>
                    </div>
                    :
                    <ContentLoader />
                }
            </React.Fragment>
        )
    }
}
export default connect(
    state => ({
        ...state.reducer.invoiceHistory,
        ...state.reducer.user,
        division_value: state.reducer.home.division_value,
        division_data: state.reducer.home.division_data,
    }),
    dispatch => ({ dispatch })
)(Invoice);
