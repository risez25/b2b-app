import React, { Component, Suspense, lazy } from 'react';

//UI
import { Button } from '@material-ui/core';
//libraray
import { connect } from 'react-redux';
import { getEveryApi } from '../../actions/actions';
import { setGlobalParam } from '../../reducers/reducers';
import ContentLoader from '../../components/global/loaders/ContentLoader';
//component
// import _table from "./_table.home";
const component = '_tabs.home'
class Group extends Component {
    onTabClick = (index) => (e) => {
        const { dispatch } = this.props;
        dispatch({
            type: setGlobalParam,
            storage: 'home.tabs_index',
            response: index,
            component:component
        });
    }
    render() {
        const { tabs_index, group_data } = this.props;
        return (
            <div className="iron-tab-bar mb-30 mb-sm-60 text-center">
                <Button
                    variant={tabs_index === 0 ? "contained" : "text"}
                    onClick={this.onTabClick(0)}
                    className={`mr-10 iron-tab-btn ${tabs_index === 0 ? "active" : ""}`}
                >
                    {group_data.label_01}
                </Button>
                <Button
                    variant={tabs_index === 1 ? "contained" : "text"}
                    onClick={this.onTabClick(1)}
                    className={`mr-10 iron-tab-btn ${tabs_index === 1 ? "active" : ""}`}
                >
                    {group_data.label_02}
                </Button>
                <Button
                    variant={tabs_index === 2 ? "contained" : "text"}
                    onClick={this.onTabClick(2)}
                    className={`mr-10 iron-tab-btn ${tabs_index === 2 ? "active" : ""}`}
                >
                    {group_data.label_03}
                </Button>

            </div>
        )
    }
}
export default connect(
    state => ({ ...state.reducer.home }),
    dispatch => ({ dispatch })
)(Group);
