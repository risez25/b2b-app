import React, { Component, Suspense, lazy } from 'react';

//UI
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
//libraray
import { connect } from 'react-redux';
import treeChanges from "tree-changes";
import { Helmet } from "react-helmet";
//component
import _groupHome from './_group.home';
import ContentLoader from '../../components/global/loaders/ContentLoader';
import _divisionlistHome from './_divisionlist.home';
import { InstantSearch } from 'react-instantsearch-dom';
import AppConfig from '../../constants/AppConfig';
import { getEveryApi } from '../../actions/actions';
import _tabsHome from './_tabs.home';
import { setGlobalParam } from '../../reducers/reducers';
const component = 'index.home'
class Home extends Component {
    componentDidMount() {
        const { dispatch, division_data, division_value, customer_id } = this.props;
        if (division_data == null) {
            dispatch(getEveryApi({
                api: `B2b/GetDivision&customer_id=${customer_id}`,
                storage: 'home.division_data',
                component: component
            }));
        }

        let division_id = division_value == 0 ? division_data[0].id : division_value
        dispatch(getEveryApi({
            api: `B2b/GetItemGroup&division_id=${division_id}`,
            storage: 'home.group_data',
            component: component
        }));
    }

    componentWillReceiveProps(nextProps) {
        const { dispatch } = this.props;
        const { changed } = treeChanges(this.props, nextProps);
        if (changed('division_data')) {
            dispatch(getEveryApi({
                api: `B2b/GetItemGroup&division_id=${nextProps.division_data[0].id}`,
                storage: 'home.group_data',
                component: component
            }));
        }

    }
    componentWillUnmount() {
        const { dispatch, default_state } = this.props;
        // let defaultState = {
        //     ...default_state,
        //     default_state: default_state
        // }
        dispatch({
            type: setGlobalParam,
            storage: 'home.group_data',
            response: default_state.group_data,
            component: component
        })
        dispatch({
            type: setGlobalParam,
            storage: 'home.tabs_index',
            response: default_state.tabs_index,
            component: component
        })
    }
    render() {
        const { group_data, division_data } = this.props;
        return (
            <React.Fragment>
                <Helmet>
                    <title>Home</title>
                </Helmet>
                {group_data !== null && division_data !== null ?
                    <div className="iron-home-wrap" >
                        <div className="container">
                            <div className="iron-sec-heading-wrap text-center mb-sm-60 mb-40">
                                <div className="heading-title">
                                    <h2>featured products</h2>
                                </div>
                            </div>
                            <div>
                                <_divisionlistHome />
                            </div>
                            <div>
                                <_tabsHome />
                            </div>
                            <div>
                                <_groupHome />
                            </div>

                        </div>
                    </div>
                    :
                    <ContentLoader />
                }
            </React.Fragment>
        )
    }
}
export default connect(
    state => ({
        ...state.reducer.home,
        ...state.reducer.user,
    }),
    dispatch => ({ dispatch })
)(Home);
