import React from 'react';

//UI
import {
    Button,
    Grid,
    FormControl,
    TextField,
    MenuItem,
    InputLabel,
    Select,
    FilledInput
} from '@material-ui/core';
//libraray
import { connect } from 'react-redux';

//component
import { setGlobalParam } from '../../reducers/reducers';
import { getEveryApi } from '../../actions/actions';
const component = '_divisionlist.home';
class DivisionList extends React.PureComponent {
    componentDidMount(){
        const{dispatch,division_data} =this.props;
        dispatch({
            type:setGlobalParam,
            storage:'home.division_value',
            response:division_data[0].id,
            component:component
        })
    }
    useSelectDivision = (e) => {
        const { dispatch } = this.props;
        //user state
        const { customer_id, id, role, username } = this.props;
        dispatch({
            type: setGlobalParam,
            storage: 'home.division_value',
            response: e.target.value,
            component:component
        });

        //update group categories
        dispatch(getEveryApi({
            api: `B2b/GetItemGroup&division_id=${e.target.value}`,
            storage: 'home.group_data',
            component:component
        }));

        //update cart data
        dispatch(getEveryApi({
            api: `B2b/GetCartItem&customer_id=${customer_id}&division_id=${e.target.value}`,
            storage: 'cart.cart_data',
            component:component
         }))
    }
    render() {
        const { division_data, division_value } = this.props;
        return (
            <Grid item xs={12} sm={12} md={8} lg={9}>
                <div className="stats-info d-md-flex mb-30 justify-content-between align-items-center">
                    <div className="app-selectbox-sm mb-30 mb-md-0">
                        <FormControl variant="filled" fullWidth>
                            <InputLabel htmlFor="filled-age-simple">Division</InputLabel>
                            <Select
                                value={division_value}
                                onChange={this.useSelectDivision}
                                input={<FilledInput name="Division" id="filled-division-simple" />}
                            >
                                {division_data.map(option => (
                                    <MenuItem key={option.id} value={option.id}>
                                        {option.code}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </div>
                </div>
            </Grid>
        )
    }
}
export default connect(
    state => ({ 
        ...state.reducer.home,
        ...state.reducer.user
    }),
    dispatch => ({ dispatch })
)(DivisionList);