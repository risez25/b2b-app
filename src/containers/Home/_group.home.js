import React, { Component, Suspense, lazy } from 'react';

//UI
import {
    Grid,
    Card,
    CardContent,
    Typography
} from '@material-ui/core';
//libraray
import { connect } from 'react-redux';
import { history } from '../../store';
//component
// import _table from "./_table.home";

class Group extends Component {
    useGotoProduct = (group_id) => (e) => {
        const { division_value, tabs_index } = this.props;
        let group_type = tabs_index + 1;
        history.push(`/product/${division_value}/${group_type}/${group_id}`);
    }
    render() {
        const { tabs_index, group_data, } = this.props;
        let group_list = []
        switch (tabs_index) {
            case 0:
                group_list = group_data.group_01
                break;
            case 1:
                group_list = group_data.group_02
                break;
            case 2:
                group_list = group_data.group_03
                break;
            default:
                break;
        }

        return (
            <Grid item xs={12}>
                <Grid container justify="center" spacing={2}>
                    {group_list.map(value => (
                        <Grid key={value.id} item>
                            <Card onClick={this.useGotoProduct(value.id)} >
                                <CardContent className='home-group-cardcontent'>
                                    <Typography variant="body2" component="p">
                                        {value.description}
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            </Grid>
        )
    }
}
export default connect(
    state => ({ ...state.reducer.home }),
    dispatch => ({ dispatch })
)(Group);
