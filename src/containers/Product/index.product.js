import React, { Component, Suspense, lazy } from 'react';

//UI
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
//libraray
import { connect } from 'react-redux';
//component
import ContentLoader from '../../components/global/loaders/ContentLoader';
import { getEveryApi } from '../../actions/actions';
import _cardProduct from './_card.product';
import { setGlobalParam } from '../../reducers/reducers';
import { Button } from '@material-ui/core';
import _gobackbutton from "../../components/GoBackButton/_gobackbutton";

const component = 'index.product'
class Product extends React.PureComponent {
    componentDidMount() {
        const { dispatch } = this.props;
        const { division_id, group_type, group_id } = this.props.match.params;
        //user state
        const { customer_id, id, role, username } = this.props;
        dispatch(getEveryApi({
            api: `B2b/getItem&division_id=${division_id}&customer_id=${customer_id}&group_type=${group_type}&group_id=${group_id}&offset=0`,
            storage: 'product.item_data',
            component: component
        }))
    }
    componentWillUnmount() {
        const { dispatch, default_state } = this.props;
        let defaultState = {
            ...default_state,
            default_state: default_state
        }
        dispatch({
            type: setGlobalParam,
            storage: 'product',
            response: defaultState,
            component: component
        })
    }
    render() {
        const { item_data } = this.props;
        return (
            <React.Fragment>
                {item_data !== null ?
                    <div className="product-page" >
                        <div className="container">
                            <div>
                                <_gobackbutton to={'/home'}/>
                                <_cardProduct {...this.props.match} />
                            </div>
                        </div>
                    </div>
                    :
                    <ContentLoader />
                }
            </React.Fragment>
        )
    }
}
export default connect(
    state => ({
        ...state.reducer.product,
        ...state.reducer.user
    }),
    dispatch => ({ dispatch })
)(Product);
