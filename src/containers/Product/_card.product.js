import React, { Component, Suspense, lazy } from 'react';

//UI
import { Divider, Card, CardContent, CardMedia, Grid } from "@material-ui/core";
import { Link } from 'react-router-dom';
//libraray
import { connect } from 'react-redux';
//component
import ContentLoader from '../../components/global/loaders/ContentLoader';
import { getEveryApi } from '../../actions/actions';
import pic from "../../assets/images/accessroies/a-1-a.jpg";
import { setGlobalParam } from '../../reducers/reducers';
import { history } from '../../store';
import NoImage from "../../assets/images/NoImage.jpg";
const component = '_card.product'
class CardComponent extends React.PureComponent {
    useGotoProductDetail = (item_id) => (e) => {
        const { division_id, group_type, group_id } = this.props.params;
        history.push(`/productDetail/${item_id}/${division_id}/${group_type}/${group_id}`);
    }
    render() {
        const { item_data } = this.props;
        
        return (
            <React.Fragment>
                <Grid item xs={12}>
                    <Grid container justify="center" spacing={2}>
                        {item_data.map((value, index) => {
                            let last_length = (value.item_details.length) - 1;
                            let image = new Image();
                            
                            return (
                                <Card className="card-product" onClick={this.useGotoProductDetail(value.id)}>
                                    <CardMedia
                                        className='card-image'
                                        component="img"
                                        image={ value.picture !=''? `data:image/png;base64,${value.picture}` : NoImage}
                                    />
                                    <CardContent className="iron-product-content p-20 pt-30 border">
                                        <h5 className="text-truncate"><Link>{value.description}</Link></h5>
                                        <div className="d-flex justify-content-between align-items-center">
                                            <div className="price-wrap">
                                                <span>RM{parseFloat(value.item_details[0].usernumber_01).toFixed(2)} - RM{parseFloat(value.item_details[last_length].usernumber_01).toFixed(2)}</span>
                                            </div>
                                        </div>
                                    </CardContent>
                                </Card>

                            )
                        })}
                    </Grid>
                </Grid>
            </React.Fragment >
        )
    }
}
export default connect(
    state => ({ ...state.reducer.product }),
    dispatch => ({ dispatch })
)(CardComponent);
