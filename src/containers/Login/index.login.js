import React, { Component, Suspense, lazy } from 'react';

//UI
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { Paper, Card, CardHeader, CardContent, TextField, FormControlLabel, Checkbox, Button } from "@material-ui/core";
//libraray
import { connect } from 'react-redux';
import treeChanges from "tree-changes";
import AES from "crypto-js/aes";
//component
import { postEveryApi, getEveryApi } from '../../actions/actions';
import { setGlobalParam } from '../../reducers/reducers';
import { history } from '../../store';

const component = 'index.login.js';
class Login extends Component {
    handleForm = (field) => (e) => {
        const { dispatch } = this.props;
        switch (field) {
            case 'is_remember':
                dispatch({
                    type: setGlobalParam,
                    storage: `login.${field}`,
                    response: !e.target.value
                })
                break;
            default:
                dispatch({
                    type: setGlobalParam,
                    storage: `login.${field}`,
                    response: e.target.value
                })
                break;
        }
    }
    useLogin = (e) => {
        const { dispatch, username, password, is_remember } = this.props;
        const rememberMe = is_remember ? 1 : 0;
        let bodyFormData = new FormData();
        // dispatch(getEveryApi({
        //     api:`B2b/isAuthenticated?username=${username}&password=${password}&rememberMe=${rememberMe}`
        // }))
        bodyFormData.set('UserLogin[username]', username);
        bodyFormData.set('UserLogin[password]', password);
        bodyFormData.set('UserLogin[rememberMe]', rememberMe);
        dispatch(postEveryApi({
            api: 'B2b/Login',
            data: bodyFormData,
            storage: 'login.login_response'
        }))
    }

    componentWillReceiveProps(nextProps) {
        const { dispatch } = this.props;
        const { changed } = treeChanges(this.props, nextProps);
        if (changed('login_response')) {
            switch (nextProps.login_response.STATUS) {
                case 'OK':
                    // const encrypt_password = AES.encrypt(nextProps.password);
                    let user = {
                        id: nextProps.login_response.id,
                        role: nextProps.login_response.role,
                        username: nextProps.login_response.username
                    }

                    dispatch({
                        type:setGlobalParam,
                        storage:'user',
                        response:user
                    })

                    const ciphertext = AES.encrypt(`${nextProps.password}`, 'B2B');
                    localStorage.setItem('username', nextProps.username)
                    localStorage.setItem('password', ciphertext)
                    localStorage.setItem('is_remember', nextProps.is_remember)
                    history.push('/home');
                    break;
                default:
                    break;
            }
        }
    }
    render() {
        const { username, password, is_remember } = this.props;
        return (
            <React.Fragment>
                <Grid container alignItems='center' alignContent='center' justify='center' style={{ height: '100vh' }} >
                    <Grid item xs={8}>
                        <Card>
                            <h3 className='login-header bg-primary'>BizOrder</h3>
                            <CardContent>
                                <form>
                                    <TextField
                                        label="Name"
                                        value={username}
                                        onChange={this.handleForm('username')}
                                        margin="normal"
                                        fullWidth
                                        autoFocus
                                    />
                                    <TextField
                                        label="Password"
                                        value={password}
                                        onChange={this.handleForm('password')}
                                        margin="normal"
                                        fullWidth
                                        type='password'
                                    />
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                checked={is_remember}
                                                color="primary"
                                                onChange={this.handleForm('is_remember')}
                                            />
                                        }
                                        label='Remember Me' />
                                    <Button fullWidth color='primary' onClick={this.useLogin}>Login</Button>
                                </form>

                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </React.Fragment>
        )
    }
}
export default connect(
    state => ({ ...state.reducer.login }),
    dispatch => ({ dispatch })
)(Login);
