export const home = {
    group_data: null,
    tabs_index: 0,
    division_data: null,
    division_value: 0,
    default_state: {
        group_data: null,
        tabs_index: 0,
        division_data: null,
        division_value: 0,
    }
}