export const cart = {
    cart_data: null,
    branch_id:0,
    branch_data:null,
    total_price: 0,
    total_price_tax: 0,
    on_delete_item: null,
    is_item_delete: false,
    is_item_update: false,
    is_cart_update: false,
    is_place_order: false,
    default_state: {
        cart_data: null,
        total_price: 0,
        total_price_tax: 0,
        on_delete_item: null,
        is_item_delete: false,
        is_item_update: false,
        is_cart_update: false,
        is_place_order: false
    }
}