export const productDetail = {
    product_detail_data: null,
    product_unit: [],
    is_add_to_cart: false,
    default_state: {
        product_detail_data: null,
        product_unit: [],
        is_add_to_cart: false,
    }
}