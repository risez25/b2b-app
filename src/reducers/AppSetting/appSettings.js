import AppConfig from "../../constants/AppConfig";
import { currencies, currencyUnicode } from "../../assets/data/currencydata";
import { languages } from "../../assets/data/localeData";
export const appSettings = {
    currencies,
    selectedCurrency: currencies[1],
    currencyUnicode,
    languages,
    selectedLocale: AppConfig.locale,
    showAlert: false,
    alertType: 'success',
    alertMessage: 'Initial Message',
    rtlLayout: AppConfig.rtlLayout,
    navCollapsed: AppConfig.navCollapsed,
    client_version:null,
    server_version:null,
    user:null
}