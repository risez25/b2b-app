let cartData = [
    {
        productID: 51,
        image: "men/1-item-a.jpg",
        name: 'denim pullover',
        price: 37.03,
        quantity: 1,
        totalPrice: 37.03
    },
    {
        productID: 52,
        image: "men/2-item-a.jpg",
        name: 'super jacket',
        price: 90,
        quantity: 1,
        totalPrice: 90
    }
]
let wishlistData = [
    {
        productID: 51,
        image: "women/15-item-a.jpg",
        name: 'long dress',
        price: 60,
        quantity: 1,
        totalPrice: 60
    }
]
let collaborationData = [
    {
        id: 1,
        image: "user-2.jpg",
        name: "Lissa Roy",
        email: "lissa@example.com",
        access: "Read"
    },
    {
        id: 2,
        image: "user-3.jpg",
        name: "Jaswinder Kaur",
        email: "jass@example.com",
        access: "Admin"
    },
    {
        id: 3,
        image: "user-2.jpg",
        name: "John Doe",
        email: "John@example.com",
        access: "Read"
    },
    {
        id: 4,
        image: "user-1.jpg",
        name: "Ritesh Bajaj",
        email: "ritesh@example.com",
        access: "Write"
    },
    {
        id: 5,
        image: "user-5.jpg",
        name: "Dimple Bhagtani",
        email: "dimple@example.com",
        access: "Admin"
    },
    {
        id: 6,
        image: "user-3.jpg",
        name: "Sam Akhtar",
        email: "akhtar@example.com",
        access: "Admin"
    }
]
const TAX = 11.37;
const SHIPPING = 3.37;

export const ecommerce = {
    cart: cartData,
    wishlist: wishlistData,
    tax: TAX,
    shipping: SHIPPING,
    receiptProducts: null,
    collaborationData: collaborationData,
}