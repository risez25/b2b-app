import "babel-polyfill";
import { appSettings } from "./AppSetting/appSettings";
import { ecommerce } from "./Ecommerce/ecommerce";
import { home } from "./Home/home.state";
import { product } from "./Product/product.state";
import { productDetail } from "./ProductDetail/productDetail.state";
import { cart } from "./Cart/cart.state";
import { invoiceHistory } from "./InvoiceHistory/invoiceHistory.state";
import { orderStatus } from "./OrderStatus/orderStatus.state";
import { openbills } from "./OpenBills/openbills.state";
import { login } from "./Login/login.state";
import { user } from "./User/user.state";
import { orderdetail } from "./OrderDetail/orderdetail.state";
import { invoicedetail } from "./InvoiceDetail/invoicedetail.state";
export const setGlobalParam = 'setGlobalParam->GlobalReducer';
const initialStates = {
    appSettings,
    ecommerce,
    home,
    product,
    productDetail,
    cart,
    invoiceHistory,
    orderStatus,
    openbills,
    login,
    orderdetail,
    invoicedetail,
    user
}

export default function reducer(
    state = {
        ...initialStates
    }
    , action) {
    switch (action.type) {
        case setGlobalParam: {
            let storage = action.storage;
            if (action.storage.indexOf('.') > -1) {
                let storage_split = storage.split('.');
                let storage_global = storage_split[0];
                let storage_object = storage_split[1];
                if (storage_split.length > 2) {
                    let storage_object_1 = storage_split[2];
                    return {
                        ...state,
                        [storage_global]: {
                            ...state[storage_global],
                            [storage_object]: {
                                ...state[storage_global][storage_object],
                                [storage_object_1]: action.response
                            }
                        }
                    }
                }

                return {
                    ...state,
                    [storage_global]: {
                        ...state[storage_global],
                        [storage_object]: action.response
                    }
                }
            }

            return {
                ...state,
                [action.storage]: action.response
            }
        }
        default: {
            return state;
        }
    }
}


