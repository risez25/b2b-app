/**
 * App Constants
*/

export default {
   AppLogo: require('../assets/images/header-logo.png'),         // App logo
   rtlLayout: false,                                             // RTL Layout
   adminLayout: false,                                            // Admin Layout
   navCollapsed: false,                                          // Sidebar Nav Layout
   algoliaConfig: {                                              // Algolia configuration
      appId: 'ZO2KPLI5OX',
      apiKey: '539028c9181f79ea269af9cf3f54772c',
      indexName: 'dev_b2b'
   },
   // Default locale
   locale: {
      locale: 'en',
      name: 'English',
      icon: 'en',
   },
   // Footer about description
   AboutUs: 'Here you can use rows and columns here to organize your footer content.Lorem ipsum dolor sit amet,consectetur adipisicing elit. Here you can use rows and columns here to organize your footer content. Lorem ipsum dolor sit amet,        consectetur adipisicing elit.',
   // Copyright text
   CopyrightText: '© All Rights Reversed | Made With Love by IRON NETWORK for better Web ',
   
}
export const dateFormat= 'DD-MM-YYYY';