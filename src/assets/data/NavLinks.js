/**
 *  header-menu and sidebar menu data
 */
/* eslint-disable */
export default [
   {
      "menu_title": "menu.Home",
      "type": "subMenu",
      "path": "/home",
      "icon": "home"
   },
   {
      "menu_title": "menu.orderstatus",
      "path": "/orderStatus",
      "icon": "home"
   },
   {
      "menu_title": "menu.invoicehistory",
      "path": "/invoiceHistory",
      "icon": "home"
   },
   {
      "menu_title": "menu.openbill",
      "path": "/openBills",
      "icon": "home"
   },
]
