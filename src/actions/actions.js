export const getEverySaga = 'getEverySaga->Saga';
export const getLatestSaga = 'getLatestSaga->Saga';
export const postEverySaga = 'postEverySaga->Saga';
export const postLatestSaga = 'postLatestSaga->Saga';
export const putEverySaga = 'putEverySaga->Saga';
export const putLatestSaga = 'putLatestSaga->Saga';
export const delEverySaga = 'delEverySaga->Saga';
export const delLatestSaga = 'delLatestSaga->Saga';
export const downLatestSaga = 'downLatestSaga->Saga';
export const uploadLatestSaga = 'uploadLatestSaga->Saga';

// everyApi is if we need to call many api in one time
// latestApi is if we need to call api one at a time

export const getEveryApi = ({
    api,
    headers = {
        'Access-Control-Allow-Origin': '*',
        'Authorization': localStorage.getItem('token'),
        'Content-Type': 'application/json'
    },
    storage = null,
    nextRoute = null,
    showLoadingStorage = null,
    errorStorage = '',
    isAlert = false,
    successAlert = null,
    errorAlert = null,
    component = ''
}) => {
    return ({
        type: getEverySaga,
        api: api,
        headers: headers,
        storage: storage,
        nextRoute: nextRoute,
        showLoadingStorage: showLoadingStorage,
        errorStorage: errorStorage,
        isAlert: isAlert,
        successAlert: successAlert,
        errorAlert: errorAlert,
        component:component,
    })
};

export const getLatestApi = ({
    api,
    headers = { 'Authorization': localStorage.getItem('token'), 'Content-Type': 'application/json' },
    storage = null,
    nextRoute = null,
    showLoadingStorage = null,
    errorStorage = '',
    isAlert = false,
    successAlert = null,
    errorAlert = null,
    component = ''
}) => {
    return ({
        type: getLatestSaga,
        api: api,
        headers: headers,
        storage: storage,
        nextRoute: nextRoute,
        showLoadingStorage: showLoadingStorage,
        errorStorage: errorStorage,
        isAlert: isAlert,
        successAlert: successAlert,
        errorAlert: errorAlert,
        component:component,
    })
};

export const postEveryApi = ({
    api,
    headers = { 
        'Content-Type': 'multipart/form-data' 
    },
    data = {},
    storage = null,
    nextRoute = null,
    showLoadingStorage = null,
    errorStorage = '',
    isAlert = false,
    successAlert = null,
    errorAlert = null,
    component = ''
}) => {
    return ({
        type: postEverySaga,
        api: api,
        headers: headers,
        data: data,
        storage: storage,
        nextRoute: nextRoute,
        showLoadingStorage: showLoadingStorage,
        errorStorage: errorStorage,
        isAlert: isAlert,
        successAlert: successAlert,
        errorAlert: errorAlert,
        component:component,
    })
};

export const postLatestApi = ({
    api,
    headers = {
        'Authorization': localStorage.getItem('token'),
        'Content-Type': 'application/json'
    },
    data = {},
    storage = null,
    nextRoute = null,
    showLoadingStorage = null,
    errorStorage = '',
    isAlert = true,
    successAlert = null,
    errorAlert = null,
    component = ''
}) => {
    return ({
        type: postLatestSaga,
        api: api,
        headers: headers,
        data: data,
        storage: storage,
        nextRoute: nextRoute,
        showLoadingStorage: showLoadingStorage,
        errorStorage: errorStorage,
        isAlert: isAlert,
        successAlert: successAlert,
        errorAlert: errorAlert,
        component:component,
    })
};

export const putEveryApi = ({
    api,
    headers = { 'Authorization': localStorage.getItem('token'), 'Content-Type': 'application/json' },
    data = {},
    storage = null,
    nextRoute = null,
    showLoadingStorage = null,
    errorStorage = '',
    isAlert = true,
    component = ''
}) => {
    return ({
        type: putEverySaga,
        api: api,
        headers: headers,
        data: data,
        storage: storage,
        nextRoute: nextRoute,
        showLoadingStorage: showLoadingStorage,
        errorStorage: errorStorage,
        isAlert: isAlert
    })
};

export const putLatestApi = ({
    api,
    headers = { 'Authorization': localStorage.getItem('token'), 'Content-Type': 'application/json' },
    data = {},
    storage = null,
    nextRoute = null,
    showLoadingStorage = null,
    errorStorage = '',
    isAlert = true,
    component = ''
}) => {
    return ({
        type: putLatestSaga,
        api: api,
        headers: headers,
        data: data,
        storage: storage,
        nextRoute: nextRoute,
        showLoadingStorage: showLoadingStorage,
        errorStorage: errorStorage,
        isAlert: isAlert
    })
};

export const delEveryApi = ({
    api,
    headers = { 'Authorization': localStorage.getItem('token'), 'Content-Type': 'application/json' },
    storage = null,
    nextRoute = null,
    showLoadingStorage = null,
    errorStorage = '',
    isAlert = false,
    successAlert = null,
    errorAlert = null,
    component = ''
}) => {
    return ({
        type: getLatestSaga,
        api: api,
        headers: headers,
        storage: storage,
        nextRoute: nextRoute,
        showLoadingStorage: showLoadingStorage,
        errorStorage: errorStorage,
        isAlert: isAlert,
        successAlert: successAlert,
        errorAlert: errorAlert,
        component:component,
    })
};

export const delLatestApi = ({
    api,
    headers = { 'Authorization': localStorage.getItem('token'), 'Content-Type': 'application/json' },
    storage = null,
    nextRoute = null,
    showLoadingStorage = null,
    errorStorage = '',
    isAlert = false,
    successAlert = null,
    errorAlert = null,
    component = ''
}) => {
    return ({
        type: getLatestSaga,
        api: api,
        headers: headers,
        storage: storage,
        nextRoute: nextRoute,
        showLoadingStorage: showLoadingStorage,
        errorStorage: errorStorage,
        isAlert: isAlert,
        successAlert: successAlert,
        errorAlert: errorAlert,
        component:component,
    })
};

export const downLatestApi = ({
    api,
    storage = null,
    nextRoute = null,
    showLoadingStorage = null,
    downloadTimestampStorage = null,
    isAlert = false,
    fileName = '',
    component = ''
}) => {
    return ({
        type: downLatestSaga,
        api: api,
        fileName: fileName,
        storage: storage,
        nextRoute: nextRoute,
        showLoadingStorage: showLoadingStorage,
        downloadTimestampStorage: downloadTimestampStorage,
        isAlert: isAlert
    })
};