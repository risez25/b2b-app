// Import redux modules, logger and devtool extension
import { createStore } from 'redux';
import { createLogger } from "redux-logger";
import { composeWithDevTools } from 'redux-devtools-extension';

// Import middleware creator
import { applyMiddleware } from 'redux';
import createSagaMiddleware from "redux-saga";
import indexReducers from "./reducers/indexReducers";
import indexSagas from "./sagas/indexSagas";
import { routerMiddleware } from "react-router-redux";
import { createBrowserHistory, createHashHistory } from "history";

export const history = createHashHistory();
const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];
if (process.env.NODE_ENV !== 'production') {
    middlewares.push(createLogger());
}

const store = createStore(
    indexReducers,
    composeWithDevTools(
        applyMiddleware(
            ...middlewares,
            routerMiddleware(history),
        )
    )
);

sagaMiddleware.run(indexSagas);

export default store;