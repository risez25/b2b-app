import axios from 'axios';
import { toast } from 'react-toastify';
import CryptoJS from "crypto-js";

export const endpoint = process.env.NODE_ENV =='development' ? 'http://localhost/scm/index.php?r=' 
: window.location.href.split('BizOrder')[0] + 'index.php?r=';
// export const endpoint = 'http://192.168.43.92/scm/index.php?r=';



export const getApi = (api, headers) => {
    let username = ''
    let bytes = ''
    let password = ''
    let rememberMe = ''
    if (localStorage.getItem('username')) {
        username = localStorage.getItem('username');
        bytes = CryptoJS.AES.decrypt(localStorage.getItem('password'), 'B2B');
        password = bytes.toString(CryptoJS.enc.Utf8);
        rememberMe = localStorage.getItem('is_remember');    
    }
    
    return axios({
        method: 'get',
        url: `${endpoint}${api}&username=${username}&password=${password}&rememberMe=${rememberMe}`,
        // headers: headers,
        validateStatus: (status) => {
            console.log(status);

            return true;
        },
    }).then((response) => {
        console.log(response);

        return response.data;
    }).catch((error) => {
        console.log(error);
        toast.error('Network Error');
    });
};

export const postApi = (api, data, headers) => {
    let username = ''
    let bytes = ''
    let password = ''
    let rememberMe = ''
    if (localStorage.getItem('username')) {
        username = localStorage.getItem('username');
        bytes = CryptoJS.AES.decrypt(localStorage.getItem('password'), 'B2B');
        password = bytes.toString(CryptoJS.enc.Utf8);
        rememberMe = localStorage.getItem('is_remember');    
    }
    return axios({
        method: 'post',
        url: `${endpoint}${api}&username=${username}&password=${password}&rememberMe=${rememberMe}`,
        data: data,
        config: { headers },
        // headers: headers,
        validateStatus: (status) => {
            return true;
        },
    }).then((response) => {
        return response.data;
    }).catch((error) => {
        toast.error('Network Error');
    });
};

export const putApi = (api, data, headers) => {
    return axios({
        method: 'put',
        url: `${endpoint}${api}`,
        data: data,
        headers: headers,
        validateStatus: (status) => {
            return true;
        },
    }).then((response) => {
        return response.data;
    }).catch((error) => {
        toast.error('Network Error');
    });
};

export const deleteApi = (api, headers) => {
    return axios({
        method: 'delete',
        url: `${endpoint}${api}`,
        headers: {
            'Authorization': localStorage.getItem('token'),
            'Content-Type': 'application/json'
        },
        validateStatus: (status) => {
            return true;
        },
    }).then((response) => {
        return response.data;
    }).catch((error) => {
        toast.error('Network Error');
    });
};

// export const downloadApi = (api, fileName) => {
//     return axios({
//         method: 'get',
//         url: `${endpoint}${api}`,
//         headers: {
//             'Content-Type': 'attachment',
//             'filename': fileName,
//             'Authorization': localStorage.getItem('token'),
//         },
//         responseType: 'arraybuffer',
//         validateStatus: (status) => {
//             return true;
//         },
//     }).then((response) => {
//         // return response.data;
//         var blob = new Blob([response.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
//         FileSaver.saveAs(blob, fileName);
//     }).catch((error) => {
//         console.log(error);

//         // toast.error('Network Error');
//     });
// };