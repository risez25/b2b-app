import React, { Component } from 'react';

//UI
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import { createMuiTheme } from "@material-ui/core/styles";

//library
import { connect } from 'react-redux';
import { ToastContainer } from "react-toastify";
import CryptoJS from "crypto-js"; 
import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import { IntlProvider } from 'react-intl';
import MomentUtils from '@date-io/moment';
import treeChanges from "tree-changes";
//component
import _appRoute from "./routes/routes";
import _miniVariantDrawer from "./components/SideMenu/MiniVariantDrawer";
import _headerOne from "./components/layouts/headers/HeaderOne";

// css
import './lib/embryoCss.js'
import 'react-toastify/dist/ReactToastify.min.css';
import 'sweetalert2/src/sweetalert2.scss'
//themes
import primaryTheme from './themes/primaryTheme';

// App locale
import AppLocale from './lang';

import DateFnsUtils from '@date-io/date-fns';
import { getEveryApi } from './actions/actions';
import _tabBottomNavigation from './components/layouts/BottomNavigation/TabBottomNavigation';
import { history } from './store';
const component='App.js'
class App extends Component {
  state = {
    is_mobile: false
  }
  useAuthorizeUser() {
    const { dispatch } = this.props;
    let username = '';
    let bytes = '';
    let password = '';
    let rememberMe = '';

    //initialize user
    // let user = {
    //     id: '',
    //     role: '',
    //     username: '',
    // }
    // dispatch({
    //     type:setGlobalParam,
    //     storage:'appSettings.user',
    //     response:user,
    //     component
    // })

    if (localStorage.getItem('username')) {
      username = localStorage.getItem('username');
      bytes = CryptoJS.AES.decrypt(localStorage.getItem('password'), 'B2B');
      password = bytes.toString(CryptoJS.enc.Utf8);
      rememberMe = localStorage.getItem('is_remember');

      dispatch(getEveryApi({
        api: `B2b/CheckAuthentication&username=${username}&password=${password}&rememberMe=${rememberMe}`,
        storage: 'user',
        component: component
      }))
    }else{
        history.push('/login')
    }

    

  }
  componentDidMount() {
    const { dispatch, customer_id } = this.props;
    this.useAuthorizeUser();
    
  }
  componentWillReceiveProps(nextProps) {
    const { dispatch } = this.props;
    const { changed } = treeChanges(this.props, nextProps);
    if (changed('customer_id')) {
      dispatch(getEveryApi({
        api: `B2b/GetDivision&customer_id=${nextProps.customer_id}`,
        storage: 'home.division_data'
      }));
    }
  }
  useUpdateDimensions = () => {
    if (window.innerWidth > 1200) {
      this.setState({ is_mobile: false })
    } else {
      this.setState({ is_mobile: true })
    }
  }
  componentWillMount() {
    this.useUpdateDimensions();
    window.addEventListener("resize", this.useUpdateDimensions);

  }
  render() {
    const { division_data } = this.props;
    const { selectedLocale } = this.props.appSettings;
    const { is_mobile } = this.state
    const currentAppLocale = AppLocale[selectedLocale.locale];
    return (
      <div className="app">
        <MuiThemeProvider theme={primaryTheme}>
          <IntlProvider
            locale={currentAppLocale.locale}
            messages={currentAppLocale.messages}
          >
            <MuiPickersUtilsProvider utils={MomentUtils}>
              <ToastContainer
                position="bottom-center"
                autoClose={false}
                newestOnTop
                closeOnClick
                autoClose={5000}
                pauseOnHover
                rtl={false}
                pauseOnVisibilityChange
                draggable
              />
              <div className="app-body">
                {/* <_miniVariantDrawer /> */}


                <_headerOne is_mobile={is_mobile} />
                <main className="main">
                  {division_data == null ? '' : <_appRoute />}
                </main>
                <br />
                <br />
                <br />
                {is_mobile ?
                  <_tabBottomNavigation is_mobile={is_mobile} />
                  : ''
                }
              </div>
            </MuiPickersUtilsProvider>
          </IntlProvider>
        </MuiThemeProvider>
      </div>
    );
  }
}
export default connect(
  state => ({
    appSettings: state.reducer.appSettings,
    division_data: state.reducer.home.division_data,
    ...state.reducer.user
  }),
  dispatch => ({ dispatch })
)(App);