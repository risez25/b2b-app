// Include React Modules
import "babel-polyfill";
import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter, Router, Switch,BrowserRouter} from 'react-router-dom';
import {Provider} from "react-redux";
import { register, unregister } from "./serviceWorker";

// Include Store consisting of Saga (Middlewares) and Reducer (States)
import store from "./store";

// Include the Root Routes configurations loader
import InitialRoute from './routes/initialRoute.js'
import { history } from "./store";

const StartApp = () =>{
    ReactDOM.render((
        <Provider store={store}>
            <HashRouter history={history}>
                {/* <BrowserRouter> */}
                    <InitialRoute/>
                {/* </BrowserRouter> */}
            </HashRouter>
        </Provider>
    ), document.getElementById('root'));
    unregister();
}
if(window.cordova){
    document.addEventListener('deviceready',StartApp,false);
}else{
    StartApp();
}
