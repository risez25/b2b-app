import React, { Component, Suspense, lazy } from "react";
import { Switch, Route, Redirect } from 'react-router-dom';
import { history } from "../store";
//libraray
import { connect } from 'react-redux';
import Loadable from 'react-loadable';
import CryptoJS from "crypto-js";
import treeChanges from "tree-changes";
//component
import ContentLoader from '../components/global/loaders/ContentLoader';
import Home from "../containers/Home/index.home";
import Product from "../containers/Product/index.product";
import ProductDetail from "../containers/ProductDetail/index.productDetail";
import Cart from "../containers/Cart/index.cart";
import InvoiceHistory from "../containers/InvoiceHistory/index.invoiceHistory";
import IndexOrderStatus from "../containers/OrderStatus/index.orderStatus";
import indexOpenbills from "../containers/OpenBills/index.openbills";
import indexLogin from "../containers/Login/index.login";
import { endpoint } from "../api/api";
import Axios from "axios";
import Swal from "sweetalert2";
import { setGlobalParam } from "../reducers/reducers";
import { getEveryApi } from "../actions/actions";
import indexOrderdetail from "../containers/OrderDetail/index.orderdetail";
import indexInvoicedetail from "../containers/InvoiceDetail/index.invoicedetail";
const component = 'routes.js'
class loadAppRoutes extends Component {
    render() {
        return (
            <Suspense fallback={<ContentLoader />}>
                <Switch>
                    <PrivateRoute path="/home" component={Home} />
                    <PrivateRoute path="/login" component={indexLogin} />
                    <PrivateRoute path="/invoiceHistory" component={InvoiceHistory} />
                    <PrivateRoute path="/invoicedetail/:inv_code" component={indexInvoicedetail} />
                    <PrivateRoute path="/orderStatus" component={IndexOrderStatus} />
                    <PrivateRoute path="/openBills" component={indexOpenbills} />
                    <PrivateRoute path="/product/:division_id/:group_type/:group_id" component={Product} />
                    <PrivateRoute path="/productDetail/:item_id/:division_id/:group_type/:group_id" component={ProductDetail} />
                    <PrivateRoute path="/cart/:customer_id/:division_id" component={Cart} />
                    <PrivateRoute path="/orderdetail/:doc_code" component={indexOrderdetail} />
                    <Route path="empty" component={null} />
                    <Redirect from='/' to='/home' />
                </Switch>
            </Suspense>
        );
    }
};

export default connect(
    state => ({ ...state.reducer.user }),
    dispatch => ({ dispatch })
)(loadAppRoutes);

const PrivateRoute = connect(
    state => ({ 
        ...state.reducer.appSettings,
        ...state.reducer.user,
    }),
    dispatch => ({ dispatch })
)(class PrivateRoute extends Component {
    state = {
        is_auth: false
    }
    async useStoreCurrentVersionToClientApp() {
        if (!localStorage.getItem('client_version') || localStorage.getItem('client_version') =='undefined') {
            const { dispatch } = this.props;
            dispatch(getEveryApi({
                api: `B2b/GetServerVersion`,
                storage: 'appSettings.client_version',
                component:component
            }))
            // localStorage.setItem('client_version', version.data.data);
        }
    }

    async useUpdateApp() {
        const { dispatch } = this.props;
        if (localStorage.getItem('client_version')) {
            const client_version = localStorage.getItem('client_version');
            dispatch(getEveryApi({
                api: `B2b/CheckAppVersion&client_version=${client_version}`,
                storage: 'appSettings.server_version',
                component:component
            }))
        }
    }


    componentWillReceiveProps(nextProps) {
        const { dispatch } = this.props;
        const { changed } = treeChanges(this.props, nextProps);
        if (changed('client_version')) {
            localStorage.setItem('client_version', nextProps.client_version);
        }
        if (changed('server_version')) {
            const client_version = localStorage.getItem('client_version');
            
            if (parseInt(client_version) < parseInt(nextProps.server_version)) {
                Swal.fire({
                    title: '',
                    text: 'There is new version, click refresh to update your application',
                    confirmButtonText: 'Refresh',
                    type: 'info'
                }).then((result) => {
                    if (result.value) {
                        localStorage.setItem('client_version', nextProps.server_version);
                        window.location.reload();
                    }
                })
            }
            // if server less then client version so upadate the client
            if (parseInt(nextProps.server_version) < parseInt(client_version)) {
                localStorage.setItem('client_version', nextProps.server_version);
            }
        }
        //appSetting.User
        if (changed('user')) {
            if (nextProps.user != "") {
                let user = {
                    customer_id: nextProps.user.customer_id,
                    id: nextProps.user.id,
                    role: nextProps.user.role,
                    username: nextProps.user.username
                }
                dispatch({
                    type: setGlobalParam,
                    storage: 'user',
                    response: user,
                    component:component
                })
                if (this.props.url) {
                    history.push(this.props.url);
                }
            } else {
                history.push('/login');
            }
        }

        if (nextProps.id != null) {
            this.setState({ is_auth: true });
        }
    }

    

    componentDidMount() {
        this.useStoreCurrentVersionToClientApp();
        this.useUpdateApp();
        // this.useAuthorizeUser();
    }
    render() {
        
        const { component, path,customer_id } = this.props;
        const { is_auth } = this.state;
        console.log('home');
        console.log(this.props);
            
        
        if (customer_id != null) {
            return (
                <Route path={path} component={component} exact={true} />
            )
        } else {
            return (
                <ContentLoader />
            )
        }
    }
})