// Include React Modules
import React, { Component } from 'react';
//library
import { Route, Switch } from 'react-router-dom';
import CryptoJS from "crypto-js";
import Axios from 'axios';
import Swal from 'sweetalert2';
import { connect } from "react-redux";
import treeChanges from "tree-changes";
// Containers
import App from '../App'
// Routes
import { history } from '../store.js';
import indexLogin from '../containers/Login/index.login';
import ContentLoader from '../components/global/loaders/ContentLoader';
import { endpoint } from '../api/api';
import { getEveryApi } from '../actions/actions';
import { setGlobalParam } from '../reducers/reducers';

const component = 'initialRoute.js'
export default loadRootRoutes => {
    return (
        <Switch>
            {/* Routes defined in here will load into the root */}
            {/* <PrivateRoute path="/login" component={Login} exact={true} /> */}
            <PrivateRoute path="/login" component={indexLogin} />
            <Route path="/" component={App} />

        </Switch>
    );
};

const PrivateRoute = connect(
    state => ({ ...state.reducer.appSettings }),
    dispatch => ({ dispatch })
)(class PrivateRoute extends Component {
    state = {
        is_auth: false
    }
    async useStoreCurrentVersionToClientApp() {
        if (!localStorage.getItem('client_version') || localStorage.getItem('client_version') == 'undefined') {
            const { dispatch } = this.props;
            dispatch(getEveryApi({
                api: `B2b/GetServerVersion`,
                storage: 'appSettings.client_version',
                component: component
            }))
            // localStorage.setItem('client_version', version.data.data);
        }
    }

    async useUpdateApp() {
        const { dispatch } = this.props;
        if (localStorage.getItem('client_version')) {
            const client_version = localStorage.getItem('client_version');
            dispatch(getEveryApi({
                api: `B2b/CheckAppVersion&client_version=${client_version}`,
                storage: 'appSettings.server_version',
                component: component
            }))
        }
    }


    componentWillReceiveProps(nextProps) {
        const { dispatch } = this.props;
        const { changed } = treeChanges(this.props, nextProps);
        if (changed('client_version')) {
            localStorage.setItem('client_version', nextProps.client_version);
        }
        if (changed('server_version')) {
            const client_version = localStorage.getItem('client_version');

            if (parseInt(client_version) < parseInt(nextProps.server_version)) {
                Swal.fire({
                    title: '',
                    text: 'There is new version, click refresh to update your application',
                    confirmButtonText: 'Refresh',
                    type: 'info'
                }).then((result) => {
                    if (result.value) {
                        localStorage.setItem('client_version', nextProps.server_version);
                        window.location.reload();
                    }
                })
            }
            // if server less then client version so upadate the client
            if (parseInt(nextProps.server_version) < parseInt(client_version)) {
                localStorage.setItem('client_version', nextProps.server_version);
            }
        }
        if (changed('user')) {
            if (nextProps.user != "") {
                let user = {
                    customer_id: nextProps.user.customer_id,
                    id: nextProps.user.id,
                    role: nextProps.user.role,
                    username: nextProps.user.username
                }
                dispatch({
                    type: setGlobalParam,
                    storage: 'user',
                    response: user,
                    component: component
                })
                if (this.props.url) {
                    history.push(this.props.url);
                } else {
                    history.push('/home');
                }
                this.setState({ is_auth: true });
            } else {
                history.push('/login');
            }
        }
    }

    useAuthorizeUser() {
        const { dispatch } = this.props;
        let username = '';
        let bytes = '';
        let password = '';
        let rememberMe = '';

        if (localStorage.getItem('username')) {
            history.push('/home');
            // username = localStorage.getItem('username');
            // bytes = CryptoJS.AES.decrypt(localStorage.getItem('password'), 'B2B');
            // password = bytes.toString(CryptoJS.enc.Utf8);
            // rememberMe = localStorage.getItem('is_remember');
        }

        // dispatch(getEveryApi({
        //     api: `B2b/CheckAuthentication&username=${username}&password=${password}&rememberMe=${rememberMe}`,
        //     storage: 'user',
        //     component: component
        // }))

    }

    componentDidMount() {
        this.useStoreCurrentVersionToClientApp();
        this.useUpdateApp();
        this.useAuthorizeUser();
    }
    render() {
        const { component, path } = this.props;
        const { is_auth } = this.state;
        return (
            <Route path={path} component={component} exact={true} />
        )

    }
})