import "regenerator-runtime/runtime";
import { call, put, takeEvery, select, takeLatest } from 'redux-saga/effects';
import { getApi, postApi, deleteApi, putApi } from './../api/api';
import { setGlobalParam } from './../reducers/reducers';
import { toast } from 'react-toastify';
import { history } from './../store';

import {
    getEverySaga,
    getLatestSaga,
    postEverySaga,
    postLatestSaga,
    delEverySaga,
    delLatestSaga,
    putEverySaga,
    putLatestSaga,
    downLatestSaga
} from "../actions/actions";
import Swal from "sweetalert2";

export const sagas = [
    takeEvery(postEverySaga, postFunction),
    takeLatest(postLatestSaga, postFunction),
    takeEvery(getEverySaga, getFunction),
    takeLatest(getLatestSaga, getFunction),
    takeEvery(delEverySaga, delFunction),
    takeLatest(delLatestSaga, delFunction),
    takeEvery(putEverySaga, putFunction),
    takeLatest(putLatestSaga, putFunction),
];

function* getFunction({ api, headers, storage, nextRoute, showLoadingStorage, errorStorage, isAlert, successAlert, errorAlert }) {
    try {
        // function to make loading state true
        if (showLoadingStorage != null) {
            yield put({ type: setGlobalParam, storage: showLoadingStorage, response: true });
        }
        // function to call API
        const response = yield call(getApi, api, headers);

        // yield put({ type: setGlobalParam, storage: storage, response: response.data });
        if (response.success == true) {
            //if have pass storage the store at reducer
            if (storage != null) {
                yield put({ type: setGlobalParam, storage: storage, response: response.data });
            }

            if (successAlert != null) {
                //will show success toast message from frontend
                // toast.success(successAlert);
                Swal.fire(
                    `${response.message}`,
                    'success'
                )
            }

            if (isAlert) {
                //will show success toast message from backend
                Swal.fire(
                    '',
                    `${response.message}`,
                    'success'
                )
                // toast.success(response.message)
            }
        } else {
            //if error from backend
            if (errorStorage) {
                yield put({ type: setGlobalParam, storage: errorStorage, response: response.responseParams });
            }

            Swal.fire(
                '',
                response.message,
                'error'
            )
        }
        //set show loading to false
        if (showLoadingStorage != null) {
            yield put({ type: setGlobalParam, storage: showLoadingStorage, response: false });
        }
    } catch (error) {
        console.log(error);
    }
}

function* postFunction({ api, headers, storage, nextRoute, data, showLoadingStorage, errorStorage, isAlert, successAlert, errorAlert }) {
    try {
        // function to make loading state true
        if (showLoadingStorage != null) {
            yield put({ type: setGlobalParam, storage: showLoadingStorage, response: true });
        }
        // function to call API
        const response = yield call(postApi, api, data, headers);
        console.log(response);
        
        // function to make loading state false
        if (showLoadingStorage != null) {
            yield put({ type: setGlobalParam, storage: showLoadingStorage, response: false });
        }

        if (response.success == true) {
            if (storage != null) {
                yield put({ type: setGlobalParam, storage: storage, response: response.data });
            }

            if (successAlert != null) {
                //will show success toast message from frontend
                Swal.fire(
                    '',
                    successAlert,
                    'success'
                )
            }

            if (isAlert) {
                //will show success toast message from backend
                Swal.fire(
                    '',
                    response.message,
                    'success'
                )
            }

            if (nextRoute != null) {
                yield call(history.push(nextRoute));
            }
        } else {
            //if error from backend
            if (errorStorage) {
                yield put({ type: setGlobalParam, storage: errorStorage, response: response.responseParams });
            }
            Swal.fire(
                '',
                response.message,
                'error'
            )   
            // toast.error(response.message)
        }

    } catch (error) {
        console.log(error);
    }
}

function* putFunction({ api, headers, storage, nextRoute, data, showLoadingStorage, errorStorage, isAlert }) {
    try {
        if (showLoadingStorage != null) {
            yield put({ type: setGlobalParam, storage: showLoadingStorage, response: true });
        }
        const response = yield call(putApi, api, data, headers);

        if (response.success == true) {
            if (storage != null) {
                yield put({ type: setGlobalParam, storage: storage, response: response.data });
            }

            if (isAlert) {
                toast.success(response.message)
            }
        } else {
            //if error from backend
            if (errorStorage) {
                yield put({ type: setGlobalParam, storage: errorStorage, response: response.data });
            }
            toast.error(response.message)
        }

        if (showLoadingStorage != null) {
            yield put({ type: setGlobalParam, storage: showLoadingStorage, response: false });
        }

    } catch (error) {
        console.log(error);
    }
}

function* delFunction({ api, headers, storage, nextRoute, data, showLoadingStorage, errorStorage, isAlert, successAlert, errorAlert }) {
    try {
        // function to make loading state true
        if (showLoadingStorage != null) {
            yield put({ type: setGlobalParam, storage: showLoadingStorage, response: true });
        }
        // function to call API
        const response = yield call(postApi, api, data, headers);
        // function to make loading state false
        if (showLoadingStorage != null) {
            yield put({ type: setGlobalParam, storage: showLoadingStorage, response: false });
        }

        if (response.success == true) {
            if (storage != null) {
                yield put({ type: setGlobalParam, storage: storage, response: response.data });
            }

            if (successAlert != null) {
                //will show success toast message from frontend
                Swal.fire(
                    '',
                    successAlert,
                    'success'
                )
            }

            if (isAlert) {
                //will show success toast message from backend
                Swal.fire(
                    '',
                    response.message,
                    'success'
                )
            }

            if (nextRoute != null) {
                yield call(history.push(nextRoute));
            }
        } else {
            //if error from backend
            if (errorStorage) {
                yield put({ type: setGlobalParam, storage: errorStorage, response: response.responseParams });
            }
            toast.error(response.message)
        }

    } catch (error) {
        console.log(error);
    }
}

function* tokenInvalid(message) {
    yield put({ type: setGlobalParam, storage: 'is_authenticated', response: false });
    localStorage.removeItem('token');
    yield call(history.push, '/login');
    toast.error(message)
}




